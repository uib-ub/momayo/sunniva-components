/* jshint strict: true */

/**
 * Main file for the Business logic
 * @author: Hemed, Tarje, Simon
 * University of Bergen Library
 **/

var app = angular.module('marcus', [
    "checklist-model" ,
    "settings",
    "ngAnimate",
    "pascalprecht.translate",
    "monospaced.qrcode"
    ]);

//Enable HTML5 mode for browser history
app.config(['$locationProvider', function($locationProvider) {
    "use strict";
    $locationProvider.html5Mode({
        enabled : true,
        requireBase: false,
        rewriteLinks : false });
}]);

//A global variable which is used to concatenate a filter key-value pair.
//DO NOT change.
var filterKeyValueSeparator = '#';

/* jshint strict: true */

/**
 * A filter
 */
app.filter('unsafe', ['$sce', function ($sce) {
    "use strict";
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}]);

/**
 * A filter.
 */
app.filter('iif', function () {
    "use strict";
    return function(input, trueValue, falseValue) {
        return input ? trueValue : falseValue;
    };
});

/**
 * Returns the name of the filter type, to be used in styling
 * TODO modify to use mySetting (should be trivial)
 */
app.filter('filterType', ['pageSetting', function(pageSetting) {
    "use strict";
    var tags = pageSetting.tags;
    return function(filter) {
        var field = filter.split(filterKeyValueSeparator)[0];
        field = field.replace(/^-/, '');
        for (var type in tags) {
            if (tags[type].queryField === field) {
                return tags[type].css;
            }
        }
        return '';
    };
}]);

/**
 * ==============================
 * i18n - angular-translate
 * ==============================
 *
 * Needs to be in separate file because it is not compatible with unit
 * tests.
 **/
app.config(['$translateProvider', 'pageSetting', function ($translateProvider, pageSetting) {
  // add translation tables
  $translateProvider.useStaticFilesLoader({
    prefix: pageSetting.appRoot + 'locales/locale-',
    suffix: '.json'
  });
  $translateProvider.preferredLanguage('no');
  $translateProvider.fallbackLanguage('en');
  $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  $translateProvider.forceAsyncReload(true);
  $translateProvider.useMessageFormatInterpolation();
  $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
}]);

app.controller('Ctrl', ['$translate', '$scope', function ($translate, $scope) {
  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);
  };
}]);


/* 
 * Disse trigger ikke på includes!
 */

$('.ui.main-nav-dropdown.dropdown').dropdown({
    transition: 'scale',
});

$('.ui.doctype.dropdown').dropdown({
    transition: 'scale',
    context: '.search-hits',
    direction: 'downward'
});

/*
 * Lager mindre header når man scroller ned, css i menu.overrides
 */
$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 30) {
            $("#header").addClass('smaller');
        } else {
            $("#header").removeClass("smaller");
        }
    });
});

$('.search-help.ui.modal')
    .modal('attach events', '.search-help-button', 'show')
;
/* jshint strict: true */

/**
 * ==============================
 * Search Controller
 * ==============================
 **/

app.controller('searchController',
               ['$scope', '$location', '$window', '$timeout', 'blackbox', 'pageSetting', 'persistentFilter', 'persistentQuery', 'queryParameters',
                function($scope, $location, $window, $timeout, blackbox, pageSetting, persistentFilter, persistentQuery, queryParameters) {
    "use strict";

    //Declare variables
    $scope.isArray = angular.isArray;
    $scope.isString = angular.isString;
    $scope.fromSuggest = null;
    var defaultPageSize = 24;
    var defaultFrom = 0;

    /**
     * Remove some default values from URL parameters if present
     */
    var stripParams = function(params) {
        //Hide "index" from the URL params, "index" is unlikely to change.
        if ('index' in params) {
            delete params.index;
        }

        //Hide "from" from the URL params, if it has a default value (0).
        if (params.from === defaultFrom) {
            delete params.from;
        }

        //Hide "size" from the URL params, if it has a default value (10).
        if (parseInt(params.size) === defaultPageSize) {
            delete params.size;
        }

        //Hide from_suggest from users
        if ('from_suggest' in params) {
            delete params.from_suggest;
        }

        return params;
    };

    /**
     * Set URL parameters, hiding defaults
     */
    var setLocation = function(params) {
        $location.search(stripParams($.extend(true, {}, params)));
    };


    /**
     * Get Aggregation document count
     *
     * @param bucket a bucket object which is a field name from
     *   Elasticsearch response.
     * @return a document count as an integer. See ES response for
     *   details. Returns 0 if bucket null or undefined.
     **/
    $scope.getDocCount = function(bucket) {
        if (bucket) {
            if ("aggs_filter" in bucket) {
                return bucket.aggs_filter.doc_count;
            } else if ("doc_count" in bucket) {
                return bucket.doc_count;
            }
        }
        return 0;
    };

    /**
     * Decide whether a facet should be hidden from UI
     * @param bucket a bucket object (field name) from Elasticsearch response.
     * @param facet a string of field-value in the filed#value format.
     * @return true if facet has doc_count = 0 and is not selected
     */
    $scope.isHiddenFacet = function(bucket, facet) {
        if ($scope.getDocCount(bucket) === 0 ) {
            if (queryParameters.isFilterSelected(facet)) {
                return false;
            }
            return true;
        }
        return false;
    };

    /**
     * Reset search
     */
    $scope.resetSearch = function() {
        queryParameters.setVariables(null, true);
    };

    /**
     * Show/Hide search help
     */
    $scope.toggleHelp = function() {
        $('.search-help.ui.modal').modal('toggle');
    };

    $scope.getCurrentPage = function() {
        return queryParameters.getCurrentPage();
    };

    $scope.setCurrentPage = function(page) {
        queryParameters.setCurrentPage(page);
    };

    /**
     * Reset the current page to 1 and then perform search
     */
    $scope.search = function() {
        queryParameters.setCurrentPage(1);
        queryParameters.setQueryString($scope.queryString);
        $scope.triggerSearch();
    };

    var performRequest = function(params) {
        $scope.searching = true;
        // make a local copy of params (otherwise persistentFilter can cause flickering)
        var localParams = $.extend(true, {}, params);

        if (localParams.q) {
          if (persistentQuery.query() && persistentQuery.query().length > 0) {
            localParams.q += ' ' + persistentQuery.query(); }
        } else {
            localParams.q = persistentQuery.query();
        }

        // add persistent filter to query
        persistentFilter.addToFilterList(localParams.filter);

        blackbox.get(localParams, pageSetting, function(data) {
            $scope.searching = false;
            $scope.ready = true;
            //Clear suggest flag after search has been executed
            //because at this point, it is already logged in the server.
            $scope.fromSuggest = null;

            if (data) {
                $scope.results = data;
            } else {
                $scope.results = null;
                params.from = 0;
                params.size = 0;
                $("#searchController").empty(); //Empty everything in search controller using jQuery
                var alert =
                    "<div id='alert-server-status' class='ui large red message' " +
                            "<strong> Service is temporarily unavailable.</strong>" +
                    "</div>";
                $("#searchController").append(alert);
                console.log("No response from Elasticsearch. The server is unreachable");
            }
        });
    };

    /**
     * Trigger search based on scope variable values
     */
    $scope.triggerSearch = function () {
        $("html, body").animate({ scrollTop: 0 });

        //Set the response params to the browser history, this triggers the AJAX request
        setLocation(queryParameters.getParams());

        // Changing location triggers actual search, done this way to
        // avoid double requests when navigating with back/forward.
        // Seems to be no Angular method to change location without
        // triggering locationChangeStart (https://github.com/angular/angular.js/issues/1699)
    };

    /**
     * Execute search and scroll to the top of the page.
     */
    $scope.searchAndScrollToTop = function () {
        $scope.search();
        $window.scrollTo(0,0);
    };

    /**
     * Watch the location, location changes trigger searches
     */
    $scope.$on('$locationChangeStart', function(event, newUrl, oldUrl, newState, oldState) {
        if (newUrl !== oldUrl) {
            queryParameters.setVariables($location.search(), false);
            queryParameters.setFromSuggest($scope.fromSuggest);
            performRequest(queryParameters.getParams());
        }
    });

    /**
     * When query parameters change, we trigger a search
     */
    $scope.$on('queryParameterChange', function() {
        $scope.triggerSearch();
    });

    /***************************************
     ** Perform the following on page load **
     ***************************************/

    //Show loading gif
    $(".blackbox-loading").show();

    $scope.searching = true;
    $scope.ready = false;

    // init to defaults
    queryParameters.setVariables($location.search());
    var params = queryParameters.getParams();

    // init persistent filters
    persistentFilter.setUrlParams(params);

    //Get data
    performRequest(params);

    //Hide loading gif (only after first request)
    $(".blackbox-loading").hide();
}]);

/* jshint strict: true */

/**
 * A tag controller directive
 **/
app.controller('tagController', ['$scope', 'pageSetting', 'queryParameters',
                                 function($scope, pageSetting, queryParameters) {
    "use strict";
    var tags = pageSetting.tags;
    var doc = $scope.$parent.doc;
    var docTags = [];

    var getFilter = function(field, value) {
        return queryParameters.getCheckedValue(field, value);
    };

    var toArray = function (stringOrArray) {
        if (!stringOrArray || stringOrArray.length === 0) {
            return null;
        }
        if (angular.isString(stringOrArray)) {
            var tmp = [];
            tmp.push(stringOrArray);
            return tmp;
        } else if (angular.isArray(stringOrArray)) {
            return stringOrArray;
        }
        return null;
    };

    for (var tagType in tags) {
        docTags[tagType] = toArray(doc._source[pageSetting.tags[tagType].responseField]);
    }

    this.getTags = function(tagType) {
        if (tagType in docTags) {
            return docTags[tagType];
        }
        return null;
    };

    this.hasTags = function(tagType) {
        return this.getTags(tagType) !== null;
    };

    this.toggle = function(tagType, tag) {
        var field, filter;
        if (tagType in tags) {
            field = tags[tagType].queryField;
            filter = getFilter(field, tag);
            if (queryParameters.isFilterSelected(filter)) {
                queryParameters.removeFilter(filter);
            } else {
               queryParameters.addFilter(filter);
            }
        }
    };

    this.isSelected = function(tagType, tag) {
        if (tagType in tags) {
            return queryParameters.isFilterSelected(getFilter(tags[tagType].queryField, tag));
        }
        return false;
    };

    this.getIcon = function(tagType) {
        if (tagType in tags) {
            return tags[tagType].icon;
        }
        return "";
    };
    
    this.getClass = function(tagType) {
        if (tagType in tags) {
            return tags[tagType].css;
        }
        return "";
    };
}]);



/* jshint strict: true */

/**
 * Directive to show which filters are active, usage:
 *
 *   <active-filters></active-filters>
 *
 */
app.directive('activeFilters', ['queryParameters', function(qp) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        template:
        '<div ng-show="filters().length > 0" class="selected-filter">' +
            '<div ng-repeat="filter in filters()" class="ui relaxed horizontal list">' +
               '{{filter}}' +
               '<div class="item">' +
                   '<a class="filter" ng-click="removeFilter(filter)" ng-class="filter | filterType">' +
                        '<span ng-if="filter.startsWith(\'-\')">' +
                             '<span translate="filters.mustNot" style="font-weight: 100"></span>' +
                              '{{value(filter)}}&nbsp;<i class="ui delete icon"></i>' +
                        '</span>' +
                        '<span ng-if="!filter.startsWith(\'-\')">' +
                              '{{value(filter)}}&nbsp;<i class="ui delete icon"></i>' +
                        '</span>' +
                   '</a>' +
               '</div>' +
            '</div>' +
        '</div>',
        link: function(scope) {
            scope.filters = function() {
                return qp.getSelectedFilters();
            };

            scope.removeFilter = function(filter) {
                qp.removeFilter(filter);
            };

            scope.value = function(filter) {
                return qp.getFilterValue(filter);
            };
        }
    };
}]);

/* jshint strict: true */

/**
 * A directive for input dates, usage:
 *
 *   <date-range></date-range>
 *
 */
app.directive('dateRange', ['queryParameters', function(qp) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        scope: true,
        template:
        '<div class="ui form">' +
            '<div class="two fields">' +
            '<div class="field" style="padding-bottom: 0" ng-class="{error: !fromDate.valid}"">' +
                '<label>{{ \'filters.fromDate\' | translate }}</label>' +
                '<input ng-model="fromDate.string" ng-change="updateFromDate()" ng-class="{error: !fromDate.valid}" type="text" placeholder="{{ \'searchContainer.datePlaceholder\' | translate }}" title="Fra dato" />' +
            '</div>' +
            '<div class="field" ng-class="{error: !toDate.valid}">' +
                '<label>{{ \'filters.toDate\' | translate }}</label>' +
                '<input ng-model="toDate.string" ng-change="updateToDate()" ng-class="{error: !toDate.valid}" type="text"  placeholder="{{ \'searchContainer.datePlaceholder\' | translate }}" title="Til dato"/>' +
            '</div>' +
            '</div>' +
        '</div>',
        link: function(scope, element, attrs) {
            // Validate input date. Should be in the form of yyyy or yyyy-MM or yyyy-MM-dd
            // The method is called when typing, hence the idea is not to display an error
            // when user is in the process of typing
            function isValidDate (date) {
                // This is a special case, for validating leap year. e.g 2017-02-29 (which is invalid)
                // Here we are asking help from Moment library. True for strict mode.
                if(date.match(/^\d{4}-\d{2}-\d{2}$/) && !moment(date, 'YYYY-MM-DD', true).isValid()) {
                    return false;
                }
                return (
                    date.match(/^\d{0,4}$|^\d{4}-?$/) || //for yyyy
                    date.match(/^\d{4}[-]([01]|0[1-9]|1[012])-?$/)  || //for yyyy-MM
                    date.match(/^\d{4}[-](0[1-9]|1[012])[-]([0123]|0[1-9]|[12][0-9]|3[01])$/) //for yyyy-MM-dd
                );
            }

            // Check if date pattern is in the form of yyyy or yyyy-MM or yyyy-MM-dd
            function hasValidDatePattern(date) {
                return date.match(/(^\d{4})(-\d{2}){0,2}$/);
            }

            var useDate = function(input, setDate) {
                if (input.string.length === 0) {
                    input.valid = true;
                    setDate(input.string);
                    return;
                }

                // Replace these chars if found
                input.string = input.string
                    .replace('\.', '-')
                    .replace('\/', '-');

                // Decide whether to display invalid input borders or not
                isValidDate(input.string) ? input.valid = true : input.valid = false; // jshint ignore:line

                // Perform search once we have valid date and correct date pattern
                if (hasValidDatePattern(input.string) && input.valid) {
                    setDate(input.string);
                }
            };

            // Constructed as an object to allow pass by reference
            scope.fromDate = {valid: true, string: qp.getFromDate()};
            scope.toDate = {valid: true, string: qp.getToDate()};

            // If model changes, through for example reset button, update dates.
            scope.$watch(qp.getFromDate, function(newValue) {
                scope.fromDate.string = newValue;
            });

            scope.$watch(qp.getToDate, function(newValue) {
                scope.toDate.string = newValue;
            });

            scope.updateFromDate = function() {
                useDate(scope.fromDate, qp.setFromDate);
            };

            scope.updateToDate = function() {
                useDate(scope.toDate, qp.setToDate);
            };
        }
    };
}]);

/* jshint strict: true */

/**
 * A directive that changes the view of the facets based on whether
 * they are selected or not.  It takes two arguments: type and
 * data. Type is the name of the field described by the facet, and
 * data is a reference to the data. The buckets for the facet should
 * be available in data.aggregations[type].buckets.
 *
 * Usage:
 *
 *   <facet type="subject" data="ctrl.results"></facet>
 *
 */
app.directive('facet', ['queryParameters', function(qp) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        scope: {
            type: "@",
            data: "="
        },
        template:
        '<div class="ui list">' +
            '<span class="item"' + ' ng-repeat="bucket in buckets() track by bucket.key" ng-hide="count(bucket)==0 && !isSelected(bucket)">' +
                '<span ng-class="{selected: isSelected(bucket)}" ng-click="toggle(bucket)">' +
                    '<a>' +
                        '<i class="icon" ng-class="{check: isSelected(bucket), square: isSelected(bucket), add: !isSelected(bucket)}"></i>' +
                    '</a>' +
                    '<span class="facet-key">{{normalizeText(bucket.key)}}&nbsp;</span>' +
                    '<span class="doc-count">({{count(bucket) | number}})</span>' +
            '</span>' +
        '</div>',
        link: function(scope, element, attrs) {
            var value = function(bucket) {
                return qp.getCheckedValue(scope.type, bucket.key);
            };

            scope.count = function(bucket) {
                return scope.$parent.getDocCount(bucket);
            };

            scope.isSelected = function(bucket) {
                return qp.isFilterSelected(value(bucket));
            };

            scope.toggle = function(bucket) {
                if (scope.isSelected(bucket)) {
                    qp.removeFilter(value(bucket));
                } else {
                    qp.addFilter(value(bucket));
                }
            };

            scope.buckets = function(bucket) {
                if (scope.data && 'aggregations' in scope.data) {
                    if (scope.data.aggregations[scope.type]) {
                        return scope.data.aggregations[scope.type].buckets;
                    } else {
                        console.warn(scope.type + ' not in index');
                        return null;
                    }
                }
                return null;
            };

            /**
             * Normalizes long texts such that we don't display long texts in facets.
             */
            scope.normalizeText = function (val) {
                if(val.length > 26) {
                    val = val.substring(0, 26) + "..";
                }
                return val;
            };

        }
    };
}]);

app.directive('facets', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:'<div><h3 class="ui header"><i class="fa fa-calendar"></i><div class="content" translate="filters.created"></div></h3><date-range></date-range><h3 class="ui header"><i class="fa fa-certificate"></i><div class="content" translate="searchContainer.statusFilter"></div></h3><facet type="hasZoom" data="results"></facet><facet type="isDigitized" data="results"></facet><h3 class="ui header"><i class="fa fa-file-o"></i><div class="content" translate="filters.type"></div></h3><facet type="type.exact" data="results"></facet><h3 class="ui header"><i class="fa fa-tag"></i><div class="content" translate="filters.concept"></div></h3><facet type="subject.exact" data="results"></facet><h3 class="ui header"><i class="fa fa-user"></i><div class="content" translate="filters.maker"></div></h3><facet type="maker.exact" data="results"></facet><h3 class="ui header"><i class="fa fa-archive"></i><div class="content" translate="filters.partOf"></div></h3><facet type="isPartOf.exact" data="results"></facet><h3 class="ui header"><i class="fa fa-ticket"></i><div class="content" translate="filters.event"></div></h3><facet type="producedIn.exact" data="results"></facet></div>'
    };
}]);

/* jshint strict: true */

/**
 * A directive.
 */
app.directive('includeReplace', function () {
    "use strict";
    return {
        require: 'ngInclude',
        restrict: 'A', /* optional */
        link: function (scope, el, attrs) {
            el.replaceWith(el.children());
        }
    };
});

app.directive('instanceSearchControllerWrapper', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:'<div class="main-content"><div id="searchController" class="ui equal width grid container"><active-filters></active-filters><div id="active-filters-reset-help"><div class="ui grid"><div class="row" style="padding: 10px 0 0 0;"><div class="twelve wide column" style="padding: 0 0 0 0;"><div class="ui horizontal list"><div class="ui item mini mobile only" style="background-color: #5baec5; padding: 5px; font-weight: bold;"><span style="margin-left:5px;">{{results.hits.total}}</span> <span class="label" translate="searchContainer.hitsFeedback" translate-values="{val: results.hits.total}"></span></div><div class="ui item tiny mobile uihidden" style="background-color: #5baec5; padding: 5px; font-weight: bold; margin-left: 0px;"><span style="margin-left:0px;">{{results.hits.total}}</span> <span class="label" translate="searchContainer.hitsFeedback" translate-values="{val: results.hits.total}"></span></div><div class="item mobile uihidden" select-doc-per-page></div><div class="item mobile uihidden" select-sort-by></div><div class="ui item mini mobile only" select-sort-by></div></div></div><div class="four wide column"><div class="list-view-chooser"><div class="ui form"><div class="inline fields"><div class="field"><div class="ui radio checkbox"><input type="radio" checked ng-model="viewType" ng-value="thumbnail" value="thumbnail"> <label><i class="block layout icon"></i></label></div></div><div class="field"><div class="ui radio checkbox"><input type="radio" ng-model="viewType" value="list"> <label><i class="list layout icon"></i></label></div></div></div></div></div></div></div><div class="row"><div class="ui horizontal list"><div class="item"><a ng-click="resetSearch()" target="_self" title="Nullstill søk"><i class="delete circle icon"></i><span translate="searchContainer.reset"></span></a></div><div class="item"><a class="search-help-button" accesskey="72" title="Søketips"><i class="help icon"></i><span translate="searchContainer.help"></span></a></div></div></div></div></div><div class="search-help ui modal"><i class="close icon"></i><div class="header" translate="searchHint.header"></div><div class="image content"><div class="image"><i class="search icon"></i></div><div class="description"><h4 translate="searchHint.andOr"></h4><ul translate="searchHint.andOrTxt"></ul><h4 translate="searchHint.not"></h4><ul translate="searchHint.notTxt"></ul><h4 translate="searchHint.truncation"></h4><ul translate="searchHint.truncationTxt"></ul><h4 translate="searchHint.fields"></h4><span translate="searchHint.fieldsTxt"></span><h4 translate="searchHint.fuzzy"></h4><ul translate="searchHint.fuzzyTxt"></ul><h4 translate="searchHint.printInfo"></h4><ul translate="searchHint.printInfoTxt"></ul></div></div></div></div><div id="search-hits" class="row search-results" ng-show="ready" style="margin-top: 40px;" ng-cloak><div ng-if="ready && results.hits.total === 0" class="no-hits ui column"><div class="ui text ui center aligned"><div class="ui large blue message"><h1 style="font-size: 2em;" class="ui icon header" translate="results.emptyResult"></h1></div></div></div><div ng-if="ready && results.error" class="no-hits ui column"><div class="ui large red message"><div class="header">Søket ditt kan ikke utføres</div><ul class="list"><li>Det inneholder sannsynligvis disse spesialtegnene: <code> + - = &#x26;&#x26; || &#x3E; &#x3C; ! ( ) { } [ ] ^ ~ &#x22; * ? : \\ / </code></li><li>Hvis man skulle søke etter reserverte tegn, må man ha omvendt skråstrek foran. F.eks søk etter <code> D/S </code> må gjøres som <code> D\\/S </code></li></ul></div></div><div ng-show="results.hits.total > 0" class="ng-cloak column"><div class="search-hits"><div class="results-hit ng-cloak"><div class="ui container" ng-show="searching"><div class="ui segment active inverted dimmer"></div></div><div class="ui container" ng-show="searching" style="margin-top:12%; margin-bottom:60%"><div class="ui basic segment"><div class="ui active inverted dimmer"><div class="ui large loader"></div></div></div></div><div class="ui container" ng-show="!searching"><div ng-switch on="viewType"><div ng-switch-when="list"><search-hits></search-hits></div><div ng-switch-default><search-thumbs></search-thumbs></div></div></div></div></div></div></div></div>'
    };
}]);

/* jshint strict: true */

/**
 * A directive for pagination, takes the total number of items as
 * argument, usage:
 *
 *   <pagination total-items=ctrl.numberOfItems></pagination>
 *
 */
app.directive('pagination', ['queryParameters', function(qp) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            totalItems: "=",
        },
        template:
        '<div>' +
        '<div class="ui pagination menu">' +
            '<a class="item first" ng-class="{active: current() == 1}" ng-click="page(1)">1</a>' +
        '</div>' +
        '<div class="ui pagination menu" ng-show="totalPages() > 1" ng-class="{mobile: current() == 1 || current() == totalPages(), uihidden: current() == 1 || current() == totalPages()}">' +
            '<a class="item" ng-repeat="p in middlePages() track by $index" ng-click="page(p)" ng-class="{active: p == current(), mobile: p != current() || p == 1, uihidden: p != current() || p == 1}">{{p}}</a>' +
        '</div>' +
        '<div class="ui pagination menu" ng-show="totalPages() > 4 && totalPages() - current() > 2">' +
            '<a class="item last" ng-class="{active: current() == totalPages()}" ng-click="page(totalPages())">{{totalPages()}}</a>' +
        '</div>' +
        '<div class="ui compact icon menu right floated">' +
            '<a class="item" ng-class="{disabled: current() == 1}" ng-click="prev()"><i class="ui caret left icon"></i></a>' +
            '<a class="item" ng-class="{disabled: current() == totalPages()}" ng-click="next()"><i class="ui caret right icon"></i></a>' +
        '</div>' +
        '</div>',
        link: function(scope) {

            // needs to be a function because there are delays in getting totalItems (?)
            scope.totalPages = function() {
                return Math.floor(scope.totalItems / qp.getSize()) + 1;
            };

            scope.middlePages = function() {
                var pages = [];
                if (scope.current() > 4 && scope.current() === scope.totalPages()) {
                    pages.push(scope.current() - 3);
                }
                if (scope.current() > 3) {
                    pages.push(scope.current() - 2);
                }
                if (scope.current() > 2) {
                    pages.push(scope.current() - 1);
                }
                if (scope.current() > 1) {// && scope.current() !== scope.totalPages()) {
                    pages.push(scope.current());
                }
                if (scope.current() + 1 <= scope.totalPages()) {
                    pages.push(scope.current() + 1);
                }
                if (scope.current() + 2 <= scope.totalPages()) {
                    pages.push(scope.current() + 2);
                }
                if (scope.current() + 3 <= scope.totalPages() && scope.current() === 1)  {
                    pages.push(scope.current() + 3);
                }
                return pages;
            };

            scope.current = function() {
                return Math.floor(qp.getCurrentPage());
            };

            scope.page = function(page) {
                if (page > 0 && page <= scope.totalPages()) {
                    qp.setCurrentPage(page);
                }
            };

            scope.prev = function() {
                var prev = scope.current() - 1;
                if (prev > 0) {
                    qp.setCurrentPage(prev);
                }
            };

            scope.next = function() {
                var next = scope.current() + 1;
                if (next <= scope.totalPages()) {
                    qp.setCurrentPage(next);
                }
            };
        }
    };
}]);

app.directive('searchBox', ['$rootScope', 'suggestService', 'queryParameters', function($rootScope, suggestService, qp) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:
        '<div class="ui search">' +
            '<div class="ui massive fluid icon input">' +
                '<input class="prompt input" type="text" ng-model="query" ng-keyup="($event.keyCode === 13 || query === \'\') && search()" autofocus autocomplete="off">' +
                '<i ng-click="search()" class="search link icon"></i>' +
            '</div>' +
            '<div class="results"></div>' +
        '</div>',
        scope: {},
        link: function(scope, element) {
        	scope.query = qp.getQueryString();

        	scope.search = function() {
                qp.setQueryString(scope.query);
                element.search('hide results');
        	};

            scope.$on('queryParameterChange', function() {
                scope.query = qp.getQueryString();
            });

            element.search({
                maxResults: $(window).width() > 1024 ? 8 : 5,
                searchOnFocus: false,
                showNoResults: false,
                searchDelay: 100,
                minCharacters: 2,
                apiSettings: {
                    responseAsync: function(settings, callback) {
                        suggestService.get(scope.query, function(response) {
                            callback({ results: response.map(function(r) { return {title: r}; }) });
                        });
                    }
                },
                onSelect: function(result, response) {
                    scope.query = result.title;
                    scope.search();
                    $rootScope.$apply(); //no idea why queryParameterChange doesn't trigger $locationChangeStart
                    return true;
                }
            });
        }
    };
}]);

app.directive('searchControllerWrapper', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:'<div class="ui main-content container" ng-controller="searchController"><div id="searchController" class="ui equal width grid container"><div class="row"><div id="search-header" class="ui segments container"><div class="ui segment search-box"><search-box></search-box></div><active-filters class="ui segment"></active-filters><div class="ui segment"><div id="active-filters-reset-help"><div class="ui horizontal list"><div class="item"><a ng-click="resetSearch()" target="_self" title="Nullstill søk"><i class="delete circle icon"></i> <span translate="searchContainer.reset"></span></a></div><div class="item"><a ng-click="toggleHelp()" class="search-help-button" accesskey="72" title="Søketips"><i class="help icon"></i><span translate="searchContainer.help"></span></a></div><div class="item mobile uihidden" select-doc-per-page></div><div class="item mobile uihidden" select-sort-by></div></div><div class="ui horizontal list right floated"><div class="ui item mini horizontal red mobile only statistic"><div class="value">{{results.hits.total | number}}</div><div class="label" translate="searchContainer.hitsFeedback" translate-values="{val: results.hits.total}"></div></div><div class="ui item tiny horizontal red mobile uihidden statistic"><div class="value">{{results.hits.total | number}}</div><div class="label" translate="searchContainer.hitsFeedback" translate-values="{val: results.hits.total}"></div></div></div><div class="search-help ui modal"><i class="close icon"></i><div class="header" translate="searchHint.header"></div><div class="image content"><div class="image"><i class="search icon"></i></div><div class="description"><h4 translate="searchHint.andOr"></h4><ul translate="searchHint.andOrTxt"></ul><h4 translate="searchHint.not"></h4><ul translate="searchHint.notTxt"></ul><h4 translate="searchHint.truncation"></h4><ul translate="searchHint.truncationTxt"></ul><h4 translate="searchHint.fields"></h4><span translate="searchHint.fieldsTxt"></span><h4 translate="searchHint.fuzzy"></h4><ul translate="searchHint.fuzzyTxt"></ul><h4 translate="searchHint.printInfo"></h4><ul translate="searchHint.printInfoTxt"></ul></div></div></div><div class="list-view-chooser"><div class="ui form"><div class="inline fields"><div class="field"><div class="ui radio checkbox"><input type="radio" checked ng-model="viewType" ng-value="list" value="list"> <label><i class="list layout icon"></i></label></div></div><div class="field"><div class="ui radio checkbox"><input type="radio" ng-model="viewType" value="thumbnail"> <label><i class="block layout icon"></i></label></div></div></div></div></div></div></div></div></div><div id="search-hits" class="row search-results" ng-show="ready" ng-cloak><div ng-cloak id="facet-sidebar" class="ng-cloak four wide column tablet or lower uihidden"><div><facets></facets></div></div><div ng-if="ready && results.hits.total === 0" class="no-hits ui column"><div class="ui text ui center aligned"><div class="ui large blue message"><h1 style="font-size: 2em;" class="ui icon header" translate="results.emptyResult"></h1></div></div></div><div ng-if="ready && results.error" class="no-hits ui column"><div class="ui large red message"><div class="header">Søket ditt kan ikke utføres</div><ul class="list"><p>Du kan prøve følgende:</p><li>Sjekk at du har stavet alle ordene riktig</li><li>Søk med færre ord</li><li>Prøv andre nøkkelord</li><li>Fjern eventuelle filtre</li><li>Det inneholder ikke spesialtegnene: <code> + - = &#x26;&#x26; || &#x3E; &#x3C; ! ( ) { } [ ] ^ ~ &#x22; * ? : \\ / </code></li></ul></div></div><div ng-show="results.hits.total > 0" class="ng-cloak column"><div class="search-hits"><div class="results-hit ng-cloak"><div class="ui container" ng-show="searching"><div class="ui segment active inverted dimmer"></div></div><div class="ui container" ng-show="searching" style="margin-top:12%; margin-bottom:60%"><div class="ui basic segment" style="padding:1em 0em 1em 0em;"><div class="ui active inverted dimmer"><div class="ui large loader"></div></div></div></div><div class="ui container" ng-show="!searching"><div ng-switch on="viewType"><div ng-switch-when="thumbnail"><search-thumbs></search-thumbs></div><div ng-switch-default><search-hits></search-hits></div></div></div></div></div></div></div></div></div>'
    };
}]);

app.directive('searchHits', function() {
    "use strict";
    var style = ' style="page-break-inside: avoid;" class="ui item fade" ';
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:
        '<div class="ui divided items">' +
            '<div class="item">' +
                '<div class="ui container hitlist-settings-top">' +
                    '<pagination total-items="results.hits.total"></pagination>' +
                '</div>' +
            '</div>' +
            '<div ng-repeat-start="doc in results.hits.hits track by doc._id"></div>' +
            '<div document-hits ng-if=\'doc._type == "document"\' ' + style + '></div>' +
            '<div ng-if=\'doc._type == "agent"\' agent-hits ' + style + '></div>' +
            '<div ng-if=\'doc._source.type == "Monument" || doc._source.type == "Objekt"\' object-hits ' + style + '></div>' +
            '<div ng-if=\'doc._source.type == "Skip" || doc._source.type == "Fartøy"\' vessel-hits ' + style + '></div>' +
            '<div ng-if=\'doc._source.type == "Emne"\' concept-hits ' + style + '></div>' +
            '<div ng-if=\'doc._type == "spatialthing"\' spatialthing-hits ' + style + '></div>' +
            '<div ng-if=\'doc._type == "collection" || doc._source.type == "Manuskriptsamling"\' collection-hits ' + style + '></div>' +
            '<div ng-repeat-end></div>' +
            '<div class="item">' +
                '<div class="ui container hitlist-settings-top">' +
                    '<pagination total-items="results.hits.total"></pagination>' +
                '</div>' +
            '</div>' +
        '</div>'
    };
});

app.directive('searchInstanceBox', ['queryParameters', function(qp) {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:
        '<div class="ui search">' +
            '<div class="ui fluid icon input">' +
                '<input class="prompt input" type="text" ng-model="query" ng-keyup="($event.keyCode === 13 || query === \'\') && search()" autofocus autocomplete="off" placeholder="Søk i denne samlingen">' +
                '<i ng-click="search()" class="search link icon"></i>' +
            '</div>' +
        '</div>',
        scope: {},
        link: function(scope) {
        	scope.query = qp.getQueryString();

        	scope.search = function() {
        		qp.setQueryString(scope.query);
        	};

            scope.$on('queryParameterChange', function() {
                scope.query = qp.getQueryString();
            });
        }
    };
}]);

app.directive('searchThumbs', function() {
    "use strict";
    var style = 'style="page-break-inside: avoid;" class="ui item fade"';
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        template:'<div class="ui divided items"><div class="item"><div class="ui container hitlist-settings-top"><pagination total-items="results.hits.total"></pagination></div></div><div class="ui four doubling stackable cards"><div ng-repeat-start="doc in results.hits.hits track by doc._id"></div><div class="thumbnail card"><a class="image" target="_blank" href="{{doc._source.uri}}"><img ng-if="doc._source.img && doc._source.hasThumbnail" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.img}}"> <img ng-if="doc._source.img && !doc._source.hasThumbnail" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.img}}"> <img ng-if="!doc._source.img && doc._source.hasThumbnail" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.hasThumbnail}}"> <img ng-if="!doc._source.img && doc._source.logo" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.logo}}"><div ng-if=\'!doc._source.hasThumbnail && !doc._source.img && !doc._source.logo && (!doc._type == "collection" || !doc._source.type == "Manuskriptsamling")\' style="display: block;" class="ui image"><i style="margin-right: 0; width: 100%;" class="ui bordered inverted grey icon camera retro"></i></div><div ng-if=\'!doc._source.hasThumbnail && !doc._source.img && !doc._source.logo && (doc._type == "collection" || doc._source.type == "Manuskriptsamling")\' style="display: block;" class="ui image"><i style="margin-right: 0; width: 100%;" class="ui bordered inverted grey icon archive retro">&nbsp; {{doc._source.type}}</i></div></a><div class="content"><a target="_blank" href="{{doc._source.uri}}"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.title||doc._source.label||doc._source.name}}</a><div class="meta"><div><selectable type="maker" class="content"></selectable></div></div><div class="extra"><div class="ui horizontal list"><div ng-if="from_date" class="item event dateDigitized"><i class="fa fa-birthday-cake"></i> <span>Digitalisert {{from_date| date:\'yyyy\'}}</span></div><div ng-if="doc._source.created" class="item event dateDigitized"><i class="fa fa-calendar"></i> <span>{{doc._source.created}}</span></div><div class="item"><i ng-if="doc._source.madeAfter || doc._source.madeBefore" class="fa fa-calendar"></i> <span ng-if="doc._source.madeAfter" class="event dateDigitized"><span>{{doc._source.madeAfter}}</span></span> <span ng-if="doc._source.madeAfter || doc._source.madeBefore" class="event dateDigitized"><span>&harr;</span></span> <span ng-if="doc._source.madeBefore" class="event dateDigitized"><span>{{doc._source.madeBefore}}</span></span></div><div ng-if="to_date" class="item event dateDigitized"><span class="year{{from_date| date:\'yyyy\'}}"></span></div></div></div></div></div><div ng-repeat-end></div></div><div class="item"><div class="ui container hitlist-settings-top"><pagination total-items="results.hits.total"></pagination></div></div></div>'
    };
});

/* jshint strict: true */

/**
 * Directive for selecting docs per page in results
 * usage
 *
 *   <div select-doc-per-page></div>
 *
 */
app.directive('selectDocPerPage', ['queryParameters', 'pageSetting', function(qp, pageSetting) {
    "use strict";
    return{
        restrict: 'A',
        scope: {},
        template:
        '<span>' +
            '<label for="docPerPage">' +
                '<i class="fa fa-eye mobile only" aria-hidden="true"></i> ' +
                '<strong translate="filters.show" class="mobile uihidden"></strong>' +
            '</label> ' +
            '<select id="docPerPage" ng-model="value" ng-change="toggle()">' +
                '<option ng-repeat="option in options" value="{{option}}">{{option}}</option>' +
            '</select>' +
        '</span>',
        link: function(scope) {
            scope.options = [12, 24, 36, 56, 104];
            scope.value = qp.getSize();
            scope.toggle = function() {
                qp.setSize(scope.value);
            };
        }
    };
}]);

/* jshint strict: true */

/**
 * Directive for selecting sort-by option, usage:
 *
 *   <div select-sort-by></div>
 *
 */
app.directive('selectSortBy', ['queryParameters', 'pageSetting', function(qp, pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        scope: {},
        template:
            '<span>' +
                '<label for="sort">' +
                    '<i class="fa fa-sort mobile only" aria-hidden="true"></i> ' +
                    '<strong class="mobile uihidden" translate="filters.sort"></strong>' +
                '</label> ' +
                '<select id="sort" ng-model="value" ng-change="toggle()">' +
                    '<option ng-repeat="option in getOptions()" translate="{{option.description}}" value="{{option.value}}">' +
                '</select>' +
            '</span>',
        link: function(scope) {
            scope.value = qp.getSortBy();
            scope.getOptions = function() {
                return pageSetting.sortOptions;
            };
            scope.toggle = function() {
                qp.setSortBy(scope.value);
            };
        }
    };
}]);

/* jshint strict: true */

/**
 * A directive that toggles tags based on the tag controller.
 * usage:
 *    
 *   <selectable type="subject"></selectable>
 *
 */
app.directive('selectable', function() {
    "use strict";
    return {
        restrict: 'E',
        replace: true,
        require: '^?tagController',
        controller: 'tagController',
        controllerAs: 'ctrl',
        transclude: true,
        scope: {
            type: "@",
        },
        template: 
            '<div class="item selectable" ng-class="ctrl.getClass(type)" ng-show="ctrl.hasTags(type)">' +
                '<i ng-if="ctrl.hasTags(type)" class="ui icon" ng-class="ctrl.getIcon(type)"></i>' +
            '<div class="content">' +
                '<a ng-repeat="tag in ctrl.getTags(type) track by $index" ng-click="ctrl.toggle(type, tag)" ng-class="{selected: ctrl.isSelected(type, tag)}">' +
                   '{{tag}}' +
                   '<i class="ui delete icon" ng-show="ctrl.isSelected(type, tag)"></i>' +
                '</a>' +
            '</div>'
    };
});

/* jshint strict: true */

/**
 * Service that communicates with the search API, usage
 *
 * blackbox.get(params, facets, callback)
 *
 * The callback should take the response as argument
 */
app.factory('blackbox', ['$http', function($http) {
    "use strict";
    var get = function(params, settings, callback) {
        //Send ajax request to Blackbox
        $http({
            method: 'GET',
            url: settings.blackboxUrl + '/search?aggs=' + encodeURI(JSON.stringify(settings.facets)),
            params: params,
            cache: true
        }).then(function (response) {
            callback(response.data);
        });
    };

    return {
        get: get
    };
}]);

/* jshint strict: true */

/**
 * Can be used to define filters that are connected to specific URL
 * parameters.  The following example has the filter "-foo#bar" for
 * searches ('../search/') with no URL parameters and no filter if the
 * URL parameter "all" is specified ('../search/?all'). It also
 * specifies that the facet "baz" is not not relevant for the first
 * option. This object should be a member of the pageSetting object.
 *
 *   persistentFilterOptions: [
 *     {
 *       "urlParam": null,
 *       "filter": "-foo",
 *       "value": "bar",
 *       "description": "exclude fooBar",
 *       "excludeFacets": [baz],
 *     },
 *     {
 *       "urlParam": "all",
 *       "filter": null,
 *       "value": null,
 *       "description": "everything",
 *       "excludeFacets": [],
 *     },
 *
 */
app.factory('persistentFilter', ['pageSetting', function(pageSetting) {
    "use strict";
    var urlParams = {};
    var options = pageSetting.persistentFilterOptions;
    var currentOption = null;
    var nullOption = null;

    var getOption = function(param) {
        for (var i in options) {
            if (options[i].urlParam === param) {
                return options[i];
            }
        }
        return null;
    };

    var getFilter = function() {
        var opt = null;
        for (var p in urlParams) {
            if (urlParams.hasOwnProperty(p)) {
                opt = getOption(p);
            }
        }
        if (opt === null) {
            opt = nullOption;
        }
        if (opt && opt.filter && opt.value) {
            return opt.filter + filterKeyValueSeparator + opt.value;
        }
        return null;
    };

    nullOption = currentOption = getOption(null);

    return {
        /** 
         * This should take all the params of the current location,
         * strip off the regular query and store the relevant one in
         * the local variable urlParams. I.e. if we have parameters
         * {q: foo, bar: true, baz: true} our persistentQuery might
         * for example only be concerned with 'bar' and the other
         * parts considered the user's query. Then this function
         * should store {bar: true}, and discard the rest.
         *
         * @param params URL parameters
         * @return the URL parameters stored (i.e. strip parts not
         *   handled by this service).
         **/
        setUrlParams: function(params) {
            var opt;
            urlParams = {};
            currentOption = null;
            for (var p in params) {
                if (params.hasOwnProperty(p)) {
                    opt = getOption(p);
                    if (opt) {
                        urlParams[p] = true;
                        currentOption = opt;
                    }
                }
            }
            if (currentOption === null) {
                currentOption = nullOption;
            }
            return urlParams;
        },

        /** 
         * Get the stored parameters
         **/
        getUrlParams: function() {
            return urlParams;
        },

        /** 
         * Return the filter corresponding to the value of urlParams
         **/
        filter: function() {
            return getFilter();
        },

        /**
         * Add the persistent filter to the provided array of filters
         * @param filterList array to append filter to
         */
        addToFilterList: function(filterList) {
            var filter = getFilter();
            if (filter && filter.length > 0) {
                filterList.push(filter);
            }
        },

        /**
         * Remove the persistent filter from the provided array of filters
         * @param filterList array to remove filter from
         */
        removeFromFilterList: function(filterList) {
            var filter = getFilter();
            var i;
            if (filterList) {
                if ((i = filterList.indexOf(filter)) !== -1) {
                    filterList.splice(i, 1);
                }
            }
        },

        /**
         * Check if a given facet is relevant for the currently
         * selected option
         * @param facetName name of the facet
         * @return boolean
         */  
        hasFacet: function(facetName) {
            return currentOption.excludeFacets.indexOf(facetName) === -1;
        }
    };
}]);

/* jshint strict: true */

/**
 * Can be used to define filters that are connected to specific URL
 * parameters.  The following example has the filter "-foo#bar" for
 * searches ('../search/') with no URL parameters and no filter if the
 * URL parameter "all" is specified ('../search/?all'). It also
 * specifies that the facet "baz" is not not relevant for the first
 * option. This object should be a member of the pageSetting object.
 *
 *   persistentFilterOptions: [
 *     {
 *       "urlParam": null,
 *       "filter": "-foo",
 *       "value": "bar",
 *       "description": "exclude fooBar",
 *       "excludeFacets": [baz],
 *     },
 *     {
 *       "urlParam": "all",
 *       "filter": null,
 *       "value": null,
 *       "description": "everything",
 *       "excludeFacets": [],
 *     },
 *
 */
app.factory('persistentQuery', ['pageSetting', function(pageSetting) {
    "use strict";
    var urlParams = {};
    var options = pageSetting.persistentQueryOptions;
    var currentOption = null;
    var nullOption = null;

    var getOption = function(param) {
        for (var i in options) {
            if (options[i].urlParam === param) {
                return options[i];
            }
        }
        return null;
    };

    var getQuery = function() {
        var opt = null;
        for (var p in urlParams) {
            if (urlParams.hasOwnProperty(p)) {
                opt = getOption(p);
            }
        }
        if (opt === null) {
            opt = nullOption;
        }
        if (opt && opt.query) {
            return opt.query;
        }
        return null;
    };

    nullOption = currentOption = getOption(null);

    return {
        /** 
         * This should take all the params of the current location,
         * strip off the regular query and store the relevant one in
         * the local variable urlParams. I.e. if we have parameters
         * {q: foo, bar: true, baz: true} our persistentQuery might
         * for example only be concerned with 'bar' and the other
         * parts considered the user's query. Then this function
         * should store {bar: true}, and discard the rest.
         *
         * @param params URL parameters
         * @return the URL parameters stored (i.e. strip parts not
         *   handled by this service).
         **/
        setUrlParams: function(params) {
            var opt;
            urlParams = {};
            currentOption = null;
            for (var p in params) {
                if (params.hasOwnProperty(p)) {
                    opt = getOption(p);
                    if (opt) {
                        urlParams[p] = true;
                        currentOption = opt;
                    }
                }
            }
            if (currentOption === null) {
                currentOption = nullOption;
            }
            return urlParams;
        },

        /** 
         * Get the stored parameters
         **/
        getUrlParams: function() {
            return urlParams;
        },

        /** 
         * Return the filter corresponding to the value of urlParams
         **/
        query: function() {
            return getQuery();
        }
    };
}]);

/* jshint strict: true */

/**
 * Service that holds all the query parameters to be used in search.
 * When a parameter is set, it a 'queryParameterChange'-event is broadcast, and the search controller decides
 * if the change warrants a new request.
 */
app.factory('queryParameters', ['$rootScope', 'pageSetting', 'persistentFilter',
                                function($rootScope, pageSetting, persistentFilter) {
    "use strict";
    var state;
    var event = "queryParameterChange";

    var broadcastChange = function() {
        state.from = 0;
        $rootScope.$broadcast(event);
    };

    var broadcastPageChange = function() {
        $rootScope.$broadcast(event);
    };

    this.setVariables = function(params, broadcast) {
        /***
         * Define default values, inside function to avoid having to
         * deep copy every time
         */
        var defaultState = {
            queryString: null,
            sortBy: '',
            settingFilter: [],
            selectedFilters: [],
            fromDate: null,
            toDate: null,
            pageSize: "24",
            from: 0,
            fromSuggest: null
        };

        broadcast = typeof broadcast !== 'undefined' ? broadcast : true;

        state = defaultState;

        if (params) {
            if ("q" in params) {
                //Unescape query string so that we don't show escaped chars in the search box.
                state.queryString = unescapeQueryString(params.q);
            }

            if ("from_date" in params) {
                state.fromDate = params.from_date;
            }

            if ("to_date" in params) {
                state.toDate = params.to_date;
            }

            if ("size" in params) {
                state.pageSize = params.size;
            }

            if ("from" in params) {
                state.from = params.from;
            }

            if ("sort" in params) {
                state.sortBy = params.sort;
            }

            if ("filter" in params) {
                if (params.filter instanceof Array) {
                    state.selectedFilters = params.filter.slice(0);
                } else {
                    state.selectedFilters = [];
                    state.selectedFilters.push(params.filter);
                    params.filter = state.selectedFilters;
                }
            }
        }

        if (broadcast) {
            $rootScope.$broadcast(event);
        }
    };


    /**
     * Generate a set of URL parameters based on scope variables
     *
     * @return parameters to be passed to search API
     */
    this.getParams = function() {
        var params = {
            q: preProcessQueryString(state.queryString),
            index: pageSetting.index,
            type: pageSetting.type,
            from_date: stripEmptyString(state.fromDate),
            to_date: stripEmptyString(state.toDate),
            filter: state.selectedFilters,
            from: (this.getCurrentPage() - 1) * state.pageSize,
            size: state.pageSize,
            sort: stripEmptyString(state.sortBy),
            from_suggest: state.fromSuggest,
        };
        $.extend(params, persistentFilter.getUrlParams());
        return params;
    };

    this.getQueryString = function() {
        return state.queryString;
    };

    this.setQueryString = function(query) {
        state.queryString = query;
        broadcastChange();
    };

    this.getCurrentPage = function() {
        return (state.from/state.pageSize) + 1;
    };

    this.setCurrentPage = function(page) {
        state.from = (page - 1) * state.pageSize;
        broadcastPageChange();
    };

    this.setSelectedFilters = function(filterArray) {
        state.selectedFilters = filterArray.slice(0);
        broadcastChange();
    };

    this.getSelectedFilters = function() {
        return state.selectedFilters;
    };

    /**
     * Remove a filter in the list of selected filters
     *
     * @param item The filter string ("field#filter")
     */
    this.removeFilter = function(item) {
        if(item) {
            remove(state.selectedFilters, item);
            broadcastChange();
        }
    };

    /**
     * Add a filter to the list of selected filters
     *
     * @param item The filter string ("field#filter")
     */
    this.addFilter = function(item) {
        if (item && $.inArray(item, state.selectedFilters) === -1) {
            state.selectedFilters.push(item);
            broadcastChange();
        }
    };

    /**
     * Is a filter already selected
     *
     * @param item The filter string ("field#filter")
     * @return boolean
     */
    this.isFilterSelected = function(item) {
        return item && $.inArray(item, state.selectedFilters) !== -1;
    };

    /**
     * Get values from checkboxes.
     *
     * The method returns a string concatenation of a field and the
     * selected value.  Note that, a field and it's selected value
     * must be separated by a hash ('#'), otherwise the server will
     * not know which is which and aggregations will fail.
     *
     * @param field The field that one wants to filter on
     * @param filter The value of the field
     * @return "field#filter" or null if invalid arguments
     */
    this.getCheckedValue = function (field, filterValue) {
        if (field !== undefined && filterValue !== undefined) {
            //Separate a field and the selected value
            return field + filterKeyValueSeparator + filterValue;
        }
        return null;
    };

    /**
     * Get filter key
     * @param  filter  a filter with form "field#value"
     * @return the value before the '#' or null if invalid string
     */
    this.getFilterKey = function (filter) {
        if (filter && filter.indexOf(filterKeyValueSeparator) !== -1) {
            return filter.split(filterKeyValueSeparator)[0];
        }
        return null;
    };

    /**
     * Get filter value
     * @param filter a filter with form "field#value"
     */
    this.getFilterValue = function (filter) {
        if (filter && filter.indexOf(filterKeyValueSeparator) !== -1) {
            return filter.split(filterKeyValueSeparator)[1];
        }
        return null;
    };

    /**
     * Getters and setters for dates
     */
    this.getFromDate = function() {
        return state.fromDate;
    };
    this.setFromDate = function(date) {
        state.fromDate = date;
        broadcastChange();
    };
    this.getToDate = function() {
        return state.toDate;
    };
    this.setToDate = function(date) {
        state.toDate = date;
        broadcastChange();
    };

    this.getSortBy = function() {
        return state.sortBy;
    };

    this.setSortBy = function(sortby) {
        state.sortBy = sortby;
        broadcastChange();
    };

    this.getSize = function() {
        return state.pageSize;
    };

    this.setSize = function(size) {
        state.pageSize = "" + size;
        broadcastChange();
    };

    this.getFromSuggest = function() {
        return state.fromSuggest;
    };

    this.setFromSuggest = function(fromSuggest) {
        if (fromSuggest) {
            state.fromSuggest = true;
        } else {
            // we set it to null to avoid logging when it's inactive
            state.fromSuggest = null;
        }
        // this doesn't need to broadcast a qpChange since it can only
        // be set by the controller when a search is already triggered
    };

    this.setVariables(null);

    return this;
}]);

app.factory('suggestService', ['$http', 'pageSetting', function($http, pageSetting) {
    "use strict";
    var get = function(query, callback) {
        //Send ajax request to Blackbox
        $http({
            method: 'GET',
            url: pageSetting.blackboxUrl + '/suggest',
            params: {
            	q: query,
            	size: 10,
            	index: pageSetting.index
            },
            cache: true,
        }).then(function (response) {
            callback(response.data);
        });
    };

    return {
        get: get
    };
}]);

app.directive('agentHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui image"><div ng-if="doc._source.img" class="ui blue ribbon label">{{doc._source.type}}</div><a ng-if="doc._source.img" target="_blank" href="{{doc._source.uri}}"><img alt="{{doc._source.title}}" class="small image" src="{{doc._source.img}}"></a><div ng-if="doc._source.img" class="ui bottom attached label marcus pushup"><span class="ui floated right">{{doc._source.available}}</span></div><a ng-if="!doc._source.img" target="_blank" href="{{doc._source.uri}}"><i class="fa fa-user-circle fa-3x" aria-hidden="true"></i></a></div><div class="content"><a ng-if="doc._source.name" class="header" target="_blank" href="{{doc._source.uri}}"><span class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.name}}</span> <small ng-if="isString(doc._source.altLabel)"><br>{{doc._source.altLabel}}</small> <small ng-if="isArray(doc._source.altLabel)"><br><div class="ui horizontal list"><div ng-repeat="altLabel in doc._source.altLabel" class="item">{{altLabel}}</div></div></small></a> <a ng-if="doc._source.label" target="_blank" href="{{doc._source.uri}}" class="header"><span class="header" ng-if="doc._source.label"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.label}}</span> <small ng-if="isString(doc._source.altLabel)"><br>{{doc._source.altLabel}}</small> <small ng-if="isArray(doc._source.altLabel)"><br><div class="ui horizontal list"><div ng-repeat="altLabel in doc._source.altLabel" class="item">{{altLabel}}</div></div></small></a><div class="meta"><span ng-if="doc._source.birthYear || doc._source.deathYear" class="ui floated right"><i class="icon wait"></i> <span ng-if="doc._source.birthYear && !doc._source.birthDate">{{doc._source.birthYear}}</span> <span ng-if="doc._source.birthDate">{{doc._source.birthDate}}</span> - <span ng-if="doc._source.deathYear && !doc._source.deathDate">{{doc._source.deathYear}}</span> <span ng-if="doc._source.deathDate">{{doc._source.deathDate}}</span></span></div><div class="description"><p ng-bind-html="doc._source.description | unsafe"></p></div><div class="extra"><div ng-if="!doc._source.img" class="ui label">{{doc._source.type}}</div></div></div></div>'
    };
}]);

app.directive('collectionHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui medium image"><div ng-if="doc._source.logo || doc._source.img" style="z-index: 1;" class="ui green ribbon label">{{doc._source.type}}</div><a target="_blank" href="{{doc._source.uri}}"><img ng-if="doc._source.img && !doc._source.logo" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.img}}"> <img ng-if="!doc._source.img && doc._source.logo" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.logo}}"> <a ng-if="!doc._source.logo && !doc._source.img" target="_blank" href="{{doc._source.uri}}"><i class="fa fa-archive fa-3x" aria-hidden="true"></i></a></a></div><div class="content"><a ng-if="doc._source.title" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.title}}</a> <a ng-if="doc._source.label && !doc._source.title" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.label}}</a><div class="meta"><div ng-if="isString(doc._source.maker)"><div class="ui horizontal list"><div class="item"><i class="ui icon user"></i><div class="content">{{doc._source.maker}}</div></div></div></div><div ng-if="isArray(doc._source.maker)"><div class="ui horizontal list"><div ng-repeat="maker in doc._source.maker track by $index" class="item"><i class="ui icon user"></i><div class="content">{{maker}}</div></div></div></div><span ng-if="doc._source.created"><i class="icon wait"></i> {{doc._source.created}}</span></div><div class="description"><p ng-bind-html="doc._source.description | unsafe"></p></div><div class="extra"><div ng-if="!doc._source.hasThumbnail && !doc._source.img"><div class="ui label">{{doc._source.type}}</div></div></div></div></div>'
    };
}]);

app.directive('conceptHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui image"><a target="_blank" href="{{doc._source.uri}}"><i class="fa fa-tag fa-3x" aria-hidden="true"></i></a></div><div class="content"><a ng-if="doc._source.prefLabel" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.prefLabel}}</a><div class="meta"><div ng-if="doc._source.inScheme"><div class="content"><span class="header">Del av: {{doc._source.inScheme}}</span></div></div><span ng-if="doc._source.created"><i class="icon wait"></i> {{doc._source.created}}</span></div><div class="description"><p ng-bind-html="doc._source.description | unsafe"></p></div><div class="extra"><div class="ui label">{{doc._source.type}}</div></div></div></div>'
    };
}]);

app.directive('documentHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui medium image" style="margin-bottom: 15px;"><div style="z-index: 1;" class="ui blue ribbon label">{{doc._source.type}}</div><a target="_blank" href="{{doc._source.uri}}"><img ng-if="doc._source.img && doc._source.hasThumbnail" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.img}}"> <img ng-if="!doc._source.img && doc._source.hasThumbnail" alt="{{doc._source.title}}" class="ui fluid image" src="{{doc._source.hasThumbnail}}"><div ng-if="!doc._source.hasThumbnail && !doc._source.img" style="display: block;" class="ui image"><i style="margin-right: 0; width: 100%;" class="ui bordered inverted grey icon massive camera retro"></i></div></a></div><div class="content" ng-controller="tagController"><a class="header" target="_blank" href="{{doc._source.uri}}"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.title||doc._source.label}}</a><div class="meta"><div class="ui list"><selectable type="maker" class="content"></selectable></div></div><div class="description"><p ng-if="doc._source.description" ng-bind-html="doc._source.description | unsafe"></p></div><hr style="background-color: #d7d7d7;"><i style="right: 2em;" ng-show=\'doc._source.hasZoom == "Med DeepZoom"\' class="circular inverted red zoom icon"></i><strong>{{doc._source.identifier}}</strong><hr style="background-color: #d7d7d7;"><div class="extra"><div class="ui list"><selectable type="collection"></selectable><selectable type="spatial"></selectable><selectable type="subject"></selectable></div><div><div><div class="ui horizontal list events"><div ng-if="from_date" class="item event dateDigitized"><i class="fa fa-birthday-cake"></i> <span>Digitalisert {{from_date| date:\'yyyy\'}}</span></div><div ng-if="doc._source.created" class="item event dateDigitized"><i class="fa fa-calendar"></i> <span>{{doc._source.created}}</span></div><div class="item"><i ng-if="doc._source.madeAfter || doc._source.madeBefore" class="fa fa-calendar"></i> <span ng-if="doc._source.madeAfter" class="event dateDigitized"><span>{{doc._source.madeAfter}}</span></span> <span ng-if="doc._source.madeAfter || doc._source.madeBefore" class="event dateDigitized"><span>&harr;</span></span> <span ng-if="doc._source.madeBefore" class="event dateDigitized"><span>{{doc._source.madeBefore}}</span></span></div><div ng-if="doc._source.available" class="item event dateDigitized"><span>Tilgjengelig fra: {{doc._source.available}}</span></div><div ng-if="to_date" class="item event dateDigitized"><span class="year{{from_date| date:\'yyyy\'}}"></span></div></div></div></div></div></div></div>'
    };
}]);

app.directive('objectHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui image"><div ng-if="doc._source.img" class="ui blue ribbon label">{{doc._source.type}}</div><a ng-if="doc._source.img" target="_blank" href="{{doc._source.uri}}"><img alt="{{doc._source.title}}" class="small image" src="{{doc._source.img}}"></a> <a ng-if="!doc._source.img" target="_blank" href="{{doc._source.uri}}"><i class="ui ship icon"></i></a></div><div class="content" ng-controller="tagController"><a ng-if="doc._source.prefLabel" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.prefLabel}}</a> <a ng-if="doc._source.name" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.name}}</a> <a ng-if="doc._source.label" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.label}}</a><div class="meta"><div class="ui list"><selectable type="maker"></selectable></div></div><div class="description"><p ng-bind-html="doc._source.description | unsafe"></p></div><div class="extra" ng-init="typeController = tagControllerFactory(\'type.exact\', doc._source.type)"><div class="ui list"><selectable type="type"></selectable></div><div class="ui horizontal list"><div ng-if="doc._source.created" class="item"><i class="icon calendar"></i> {{doc._source.created}}</div></div></div></div></div>'
    };
}]);

app.directive('spatialthingHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui image"><a target="_blank" href="{{doc._source.uri}}"><i class="fa fa-map-marker fa-3x" aria-hidden="true"></i></a></div><div class="content"><a ng-if="doc._source.prefLabel" target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.prefLabel}}</a><div class="meta"><div ng-if="doc._source.inScheme"><div class="content"><span class="header">Del av: {{doc._source.inScheme}}</span></div></div><span ng-if="doc._source.created"><i class="icon wait"></i> {{doc._source.created}}</span></div><div class="description"><p ng-bind-html="doc._source.description | unsafe"></p></div><div class="extra"><div ng-if="isString(doc._source.type)"><div class="ui label">{{doc._source.type}}</div></div><div ng-if="isArray(doc._source.type)"><div><span ng-repeat="type in doc._source.type track by $index" class="ui label">{{type}}</span></div></div></div></div></div>'
    };
}]);

app.directive('vesselHits', ['pageSetting', function(pageSetting) {
    "use strict";
    return {
        restrict: 'A',
        replace: true,
        transclude: false,
        template:'<div><div class="ui image"><div ng-if="doc._source.img" class="ui blue ribbon label">{{doc._source.type}}</div><a ng-if="doc._source.img" target="_blank" href="{{doc._source.uri}}"><img alt="{{doc._source.prefLabel}}" class="small image" src="{{doc._source.logo}}"></a><div ng-if="doc._source.img" class="ui bottom attached label marcus pushup"><span class="ui floated right">{{doc._source.available}}</span><br></div><i ng-if="!doc._source.img" class="ui huge ship icon"></i></div><div class="content"><a target="_blank" href="{{doc._source.uri}}" class="header"><qrcode class="qrcode uihidden right floated" data="{{doc._source.uri}}"></qrcode>{{doc._source.prefLabel}}</a><div class="meta"><span ng-if="doc._source.birthYear || doc._source.deathYear" class="ui floated right"><i class="icon wait"></i><span ng-show="doc._source.birthYear && !doc._source.birthDate">{{doc._source.birthYear}}</span><span ng-if="doc._source.birthDate">{{doc._source.birthDate}}</span> - <span ng-if="doc._source.deathYear && !doc._source.deathDate">{{doc._source.deathYear}}</span><span ng-if="doc._source.deathDate">{{doc._source.deathDate}}</span></span></div><div class="description"><p ng-bind-html="doc._source.description | unsafe"></p></div><div class="extra"><div class="ui label">{{doc._source.type}}</div></div></div></div>'
    };
}]);
