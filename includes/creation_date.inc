{% if first.main.created.value != null %}
  <i class="ui calendar icon"></i> 
  <span itemprop="dateCreated">{{ first.main.created.value }}</span>
{% endif %}

{% if first.main.madeafter.value != null || first.main.madebefore.value != null %}
<i class="ui calendar icon"></i> 
  {% if first.main.madeafter.value != null && first.main.madebefore.value != null %}
  Laget mellom {{ first.main.madeafter.value }} og {{ first.main.madebefore.value }}
  {% endif %}
  {% if first.main.madeafter.value != null && first.main.madebefore.value == null %}
  Laget etter {{ first.main.madeafter.value }}
  {% endif %}
  {% if first.main.madeafter.value == null && first.main.madebefore.value != null %}
  Laget før {{ first.main.madebefore.value }}
  {% endif %}

{% endif %}