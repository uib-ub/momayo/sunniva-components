<div class="ui top fixed menu mobile only">
  <div class="ui item">
      <i class="sidebar icon" id="show-menu"></i>
  </div>
  <div class="uiblogo">
    <a href="http://www.uib.no/" target="_blank"><img src="{{lodspk.home}}img/uiblogo.png" alt="image" /></a>
  </div>
  <div class="item logo {% if lodspk.isAdmin %}admin{% endif %}">
    <a href="{{lodspk.home}}home">{% if lodspk.isAdmin %}{{lodspk.title}}<span style="font-family:'Roboto','Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 18px;">-admin</span>{% else %}{{lodspk.title}}{% endif %}</a>
  </div>
  <div class="right menu">  
  </div>
  <a class="item" href="{{lodspk.home}}cart"><i class="shop icon"></i><span class="simpleCart_quantity ui tiny red label"></span></a>
</div>