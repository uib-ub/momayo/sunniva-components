
<span class="header">Avbildet:</span>
  <div class="ui four doubling stackable cards" style="margin: 20px 0 20px 0 !important;">
    {% for row in models.depicted %}
    {% ifchanged row.uri.value %}
    <div class="card">
      <div class="content">
        {% if row.img %}<img class="right floated tiny ui circular image" src="{{row.img.value}}" />{% endif %}
        <a class="header" href="{{ row.uri.value }}" >{{row.label.value}}</a>
        <div class="meta">
          <p>{% if row.birthDate.value || row.deathDate.value %}
          {% if row.birthDate %}<small>({{ row.birthDate.value }}{% else %}<small>(&nbsp;&nbsp;&nbsp;&nbsp;-{% endif %}
          {% if row.deathDate %}- {{ row.deathDate.value }})</small>{% else %}-&nbsp;&nbsp;&nbsp;&nbsp;)</small>{% endif %}
          {% else %}</small>{% endif %}</p>   
        </div>
        {% if row.description %}
        <div class="description">
          <p>adfasfdadsf{{row.description.value|truncatewords:15}}</p>
        </div>
        {% endif %}
      </div>
    </div>
      {% endifchanged %}
      {% endfor %}
  </div>  