          <div style="overflow:visible !important; font-size: .7rem;" class="ui visible overlay bottom  inverted sidebar labeled icon menu mobile uihidden left-sidebar">

            <a class="item" href="{{lodspk.home}}home">
              <i class="home icon"></i> Hjem
            </a>

            <a class="item" href="{{lodspk.home}}admin/cataloguers">
              <i class="image icon"></i> Siste
            </a>

            <div class="ui dropdown item">
              <i class="flag icon"></i>Katalogisér
              <div class="menu side-nav-popout">
                <a class="item" href="http://marcus.uib.no/admin/upload.html">
                  <i class="upload icon"></i> Last opp
                </a>
                <a class="item" href="{{lodspk.home}}admin/search/?filter=isCatalogued%23Ikke%20katalogisert">
                  <i class="edit icon"></i> Registrer
                </a>
                <a class="item" href="{{lodspk.home}}admin/search/?filter=showWeb%23F">
                  <i class="newspaper icon"></i> Publiser
                </a>
                <a class="item" href="{{lodspk.home}}admin/search/?filter=hasZoom%23Med%20DeepZoom&filter=showWeb%23F">
                  <i class="gift icon"></i> Nye filer
                </a>
              </div>
            </div>

            <a class="item" href="{{lodspk.home}}admin/orders-printlabel">
              <i class="newspaper icon"></i> Etiketter
            </a>

            <div class="ui dropdown item">
              <i class="shopping bag icon"></i> Oppdrag
              <div class="menu side-nav-popout">
                <a class="item" href="{{lodspk.home}}admin/orders">
                  <i class="search icon"></i> Søk i oppdrag
                </a>
                <a class="item" href="http://oppdrag.ub.uib.no" target="_blank">
                  <i class="fa fa-bullseye fa-fw"></i> Oppdrag.uib.no
                </a>
              </div>
            </div>

            <div class="ui dropdown item">
              <i class="browser icon"></i>
              Bla i...
              <div class="menu side-nav-popout">
                <a class="item" href="{{lodspk.home}}collections">
                  <i class="grid layout icon"> </i> Samlinger
                </a>
                <a class="item" href="{{lodspk.home}}events">
                  <i class="calendar icon"></i> Hendelser
                </a>
                <a class="item" href="{{lodspk.home}}exhibitions">
                  <i class="university icon"></i> Utstillinger
                </a>
                <hr />
                <a class="item" href="{{lodspk.home}}conceptschemes">
                  <i class="tags icon"></i> Register
                </a>
                <a class="item" href="{{lodspk.home}}admin/places">
                  <i class="map pin icon"></i> Steder
                </a>
                <a class="item" href="{{lodspk.home}}admin/techniques">
                  <i class="book icon"></i> Teknikker
                </a>
                <a class="item" href="{{lodspk.home}}admin/bse-hierarchy">
                  <i class="tags icon"></i> Billedsamlingens emneregister
                </a>
                <hr />
                <a class="item" href="{{lodspk.home}}admin/worklists">
                  <i class="ordered list icon"></i> Arbeidslister
                </a>
                <a class="item" href="{{lodspk.home}}admin/acquisitions">
                  <i class="gift icon"></i> Akkvisisjoner
                </a>
                <a class="item" href="{{lodspk.home}}admin/storageunits">
                  <i class="disk outline icon"></i> Lagringsenheter
                </a>    
              </div>
            </div>

            <div class="ui dropdown item">
              <i class="line chart icon"></i> Stat
              <div class="menu side-nav-popout">
                <a class="item" href="https://stats.uib.no/index.php?module=CoreHome&action=index&idSite=23&period=range&date=previous7#?idSite=23&period=range&date=previous7&category=Dashboard_Dashboard&subcategory=1" target="_blank">
                  <i class="line chart icon"></i> Stat Marcus <i class="external alternate icon"></i>
                </a>
                <a class="item" href="{{lodspk.home}}stats">
                  <i class="line chart icon"></i> Stat datasettet
                </a>
              </div>
            </div>

            <a class="item" href="https://ubblod.disqus.com/admin/moderate/" target="_blank">
              <i class="comments icon"></i> Disqus
            </a>

            <a class="item" target="_blank" href="https://momayo.gitlab.io/how-momayo/">
              <i class="book icon"></i> Veiledning
            </a>

            <a class="item" href="http://marcus.uib.no" target="_blank">
              <i class="bullseye icon"></i> Marcus
            </a>
            <a class="item" href="https://sak.uib.no/projects/pw0/issues/new" target="_blank">
              <i class="exclamation triangle icon"></i>Feil
            </a>

        <!--  <a class="item" href="{{lodspk.home}}errors">
            <i class="exclamation triangle icon"></i> Feil
          </a>-->

        </div>