<div class="ui top fixed menu mobile hidden">
  <div class="uiblogo">
    <a href="http://www.uib.no/" target="_blank"><img src="{{lodspk.home}}img/uiblogo.png" alt="image" /></a>
  </div>
  <div class="item logo {% if lodspk.isAdmin %}admin{% endif %}">
    <a href="{{lodspk.home}}home">{% if lodspk.isAdmin %}{{lodspk.title}}<span style="font-family:'Roboto','Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 18px;">-admin</span>{% else %}{{lodspk.title}}{% endif %}</a>
  </div>

  <div class="right menu">
    <div class="item">
      <form class="ui form top-search" role="search" method="get" action="{{lodspk.home}}search">
        <input type="text" placeholder="Søk ..." name="q">
      </form>
    </div>
    <div class="ui dropdown item">Meny<i class="dropdown icon"></i>
      <div class="menu">
        <a class="item" href="{{lodspk.home}}collections"><i class="grid layout icon"></i>Samlinger</a>
        <a class="item" href="{{lodspk.home}}search?filter=type.exact%23Bilde"><i class="image icon"></i>Bildesøk</a>
        <a class="item" href="{{lodspk.home}}events"><i class="calendar icon"></i>Hendelser</a>
        <a class="item" href="{{lodspk.home}}exhibitions"><i class="university icon"></i>Utstillinger</a>
        <div class="ui divider"></div>
        <a class="item" href="{{lodspk.home}}genreform"><i class="book icon"></i>Sjanger/form</a>
        <a class="item" href="{{lodspk.home}}conceptschemes"><i class="tags icon"></i>Emneregister</a>
        <a class="item" href="{{lodspk.home}}id/49221d90-3eb7-4d2e-a215-3d4585558dea"><i class="map pin icon"></i>Sted</a>
        <div class="ui divider"></div>
        <a class="item" href="{{lodspk.home}}news"><i class="newspaper outline icon"></i>Nytt</a>
        <a class="item" href="{{lodspk.home}}articles"><i class="print icon"></i>Artikler og fagstoff</a>
        <div class="ui divider"></div>
        <a class="item" href="{{lodspk.home}}search-help"><i class="search icon"></i>Søketips</a>
        <a class="item" href="{{lodspk.home}}resources"><i class="heart icon"></i>Ressurser</a>
        <a class="item disabled" href="{{lodspk.home}}techniques"><i class="newspaper icon"></i>Teknikker</a>
        <a class="item disabled" href="{{lodspk.home}}conceptschemes"><i class="newspaper icon"></i>Andre greier</a>
      </div>
    </div>
    <div class="ui dropdown item"><i class="info icon"></i>
      <div class="menu">
        <a class="item" href="{{lodspk.home}}about-marcus">Om Marcus</a>
        <a class="item" href="{{lodspk.home}}about-collections">Om Spesialsamlingene</a>
        <a class="item" href="{{lodspk.home}}terms-of-use">Bruksvilkår</a>
        <a class="item" href="{{lodspk.home}}stats">Statistikk</a>
      </div>
    </div>
    <a class="item" href="{{lodspk.home}}/cart"><i class="shop icon"></i><span class="simpleCart_quantity ui tiny red label"></span></a>
  </div>
</div>