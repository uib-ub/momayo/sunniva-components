<div class="digital-resource"> <!-- gjelder alle -->
  {% if models.dzi|length != 0 %}
  <!-- Contains the OpenSeadragon viewer -->
  <div id="dzi-tab" data-tab="dzi" class="ui top attached tab segment no-padding active"> 
    <div id="dzi"></div>
  </div> 
  {% endif %}   

  {% if models.digitalresources|length != 0 %}  
  <div id="jpeg-tab" data-tab="jpeg" class="{% if models.dzi|length == 0 %}active{% endif %} ui top attached tab segment no-padding">

    <!-- fotorama.css & fotorama.js. -->
    <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>

    <div class="fotorama" 
    data-width="100%"
    data-ratio="800/600"
    data-minwidth="400"
    data-maxwidth="1000"
    data-minheight="40%"
    data-maxheight="100%"
    style="height: auto;">
    {% for row in models.digitalresources %}    
    <img src="{{row.imgSM.value}}" data-full="{{row.imgMD.value}}" alt=""/>
    {% endfor %}
  </div>
</div>
{% endif %}
</div>   

<div class="digital-resource-nav">   
  <div class="ui bottom attached tabular menu dr-tab">
    {% if models.digitalresources|length != 0 && models.dzi|length != 0 %}  
    <a class="item {% if models.dzi|length != 0 %}active{% else %}disabled{% endif %}" data-tab="dzi">Zoom</a>
    <a class="item {% if models.dzi|length == 0 %}active{% endif %}" data-tab="jpeg">Jpeg</a>
    {% endif %}
    {% if models.dzi|length > 1 %}
    <button class="ui item button" style="font-size: .75rem; background: #333333 !important;">gå til side &nbsp;<input style="width: 55px;" type="number" id="page" value="1"/>&nbsp; av&nbsp; <span id="tileSourcesLength"></span></button>
    {%endif%}  
    
    <div id="dzi-control" class="right menu">
      {% if models.digitalresources|length != 0 %}
        <!-- DOWNLOAD -->
        {%include "../../includes/download_button.inc"%}
      {%endif%}

      {% if models.digitalresources|length != 0 || models.dzi|length > 1 %}
      <!-- DEL -->
      <div class="del" style="margin: auto; padding: 10px;">
        <a href="https://twitter.com/intent/tweet?text={% if first.main.title != "" %}{{first.main.title.value|urlencode}}{% else %}{% if first.main.label != "" %}{{first.main.label.value|urlencode}}{%endif%}{%endif%} Fra marcus.uib.no&url={{lodspk.local.value}}" target="_blank"><i class="twitter icon"></i></a>

        <a href="https://www.facebook.com/sharer/sharer.php?u={{lodspk.local.value}}" target="_blank"><i class="facebook icon"></i></a>

        <a href="data-pin-do="buttonBookmark" "https://www.pinterest.com/pin/create/button/?text={% if first.main.title != "" %}{{first.main.title.value|urlencode}}{% else %}{% if first.main.label != "" %}{{first.main.label.value|urlencode}}{%endif%}{%endif%} Fra marcus.uib.no&url={{lodspk.local.value}}" target="_blank"><i class="pinterest icon"></i></a>
      </div>
      <script type="text/javascript">
        $(document).ready(function(){
          $(".social-sidebar").popover();   
        });
      </script>
      <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
      {% endif %}
    </div>
  </div>  
</div>

{% if models.dzi|length != 0 %} 
<script src="{{lodspk.home}}scripts/openseadragon/build/openseadragon/openseadragon.min.js"></script>
<script type="text/javascript"> 
  var viewer = OpenSeadragon({
    id:            "dzi",
    //prefixUrl:     "{{lodspk.home}}scripts/openseadragon/build/openseadragon/images/",
    prefixUrl:     "",
    tileSources:   [     
    {% for row in models.dzi %}{%if !forloop.first && models.dzi|length > 1 %},{%endif%}
    "{{ row.dziUri.value }}"{% endfor %}
    ],
    visibilityRatio: 1.0,
    minZoomLevel: 0.3,
    constrainDuringPan: true,
    preserveViewport: false,
    //minZoomImageRatio: 0.9,
    minPixelRatio: 0.5,
    animationTime: 0.8,
    springStiffness: 8,
    sequenceMode: true,
    showRotationControl: true,
    showReferenceStrip: true,
    referenceStripScroll: 'horizontal',
    navImages: {
      zoomIn: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/zoomin_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/zoomin_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/zoomin_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/zoomin_pressed.png'
      },
      zoomOut: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/zoomout_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/zoomout_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/zoomout_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/zoomout_pressed.png'
      },
      home: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/home_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/home_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/home_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/home_pressed.png'
      },
      fullpage: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/fullpage_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/fullpage_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/fullpage_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/fullpage_pressed.png'
      },
      rotateleft: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/rotateleft_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/rotateleft_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/rotateleft_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/rotateleft_pressed.png'
      },
      rotateright: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/rotateright_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/rotateright_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/rotateright_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/rotateright_pressed.png'
      },
      previous: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/previous_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/previous_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/previous_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/previous_pressed.png'
      },
      next: {
        REST:   '{{lodspk.home}}img/openseadragon-icons/next_rest.png',
        GROUP:  '{{lodspk.home}}img/openseadragon-icons/next_grouphover.png',
        HOVER:  '{{lodspk.home}}img/openseadragon-icons/next_hover.png',
        DOWN:   '{{lodspk.home}}img/openseadragon-icons/next_pressed.png'
      }
    },
    showNavigator:  true
    //navigatorId:   "navigatorDiv",
    //toolbar:       "dzi-control",
    //zoomInButton: "zoom-in",
    //zoomOutButton:  "zoom-out",
    //homeButton:     "resetZoom",
    //fullPageButton: "full-page",
    //nextButton:     "next",
    //previousButton: "previous"
  });

  $('#page').change(function() {
    chpage = $('#page').val();
    currentPage = chpage-1;
    viewer.goToPage(chpage-1); 
  });
  viewer.addHandler('page', function(event){
    $('#page').val(event.page+1);
  });
  function tileSourceLength() {
    length = viewer.tileSources.length ;
    $('#tileSourcesLength').append(length);
  };
  tileSourceLength();
</script>
  
{% endif %}  
