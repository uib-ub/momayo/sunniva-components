<span class="header">Avbildet på</span>
  <div class="ui cards">
    {% for row in models.depiction %}
    {% ifchanged row.uri.value %}
    <div class="ui card">
      <a style="min-height:250px; width: 100%;" class="ui image" href="{{ row.uri.value }}">
        <img {% if row.img %}src="{{row.img.value}}"{%else%}data-src="holder.js/350x250"{%endif%}  alt="">
      </a>
      <div class="extra content">
        <a href="{{ row.uri.value }}"><strong>{{ row.label.value }}</strong></a>
        {% if row.description %}<p>{{row.description.value|truncatewords:15}}</p>{% endif %}        
      </div>
    </div>
      {% endifchanged %}
      {% endfor %}
  </div>  
