<footer class="ui container fluid">
	<div class="fullwidth">
		<div class="ui stackable grid">
			<div class="ui row">
				<div class="four wide column">
					<h4>Om Sunniva</h4>
					<p><strong>Sunniva</strong> Id nulla dolore fugiat amet proident in ut sint ut nulla laboris irure. Sunt est ad pariatur do dolor ea amet nostrud ut magna dolor sint ullamco ea consequat in dolor pariatur incididunt adipisicing est in veniam velit ut nisi esse veniam dolore irure aute in laboris eiusmod. <a href="{{lodspk.home}}about">her...</a></p>
				</div>

				<div class="three wide column">
					<h4>Om Marcus</h4>
					<p><strong>MARCUS</strong> er Spesialsamlingene til Universitetsbiblioteket i Bergen sin portal til digitaliserte manuskript, fotografi, diplomer og mye mer. MARCUS er oppkalt etter Marcus Selmer, Bergens første fotograf. Gå til MARCUS <a href="http://marcus.uib.no/">her...</a></p>
				</div>

				<div class="three wide column">
					<h4>Skeivt arkiv</h4>
					<p>Skeivt arkiv tar vare på, dokumenterer og formidler skeiv historie eller LHBT-historie. Finn ut mer om Skeivt arkiv <a href="https://skeivtarkiv.no/" target="_blank">her...</a></p>
					<p><a href="https://skeivtarkiv.no/om-skeivt-arkiv" target="_blank">Åpningstider og kontaktinformasjon</a></p>
				</div>

				<div class="three wide column">
					<h4>Språksamlingene</h4>
					<p>Språksamlingene er en betydelig vitenskaplig ressurs som består av eldre fysisk materiale og elektronisk språklig materiale (både digitalisert og digitalt utviklet). <a href="https://www.uib.no/ub/101277/norsk-kulturarv-i-skrift-og-tale" target="_blank">(mer...)</a></p>
				</div>

			</div>

			<div class="ui row" style="border-top:1px solid #474747;">
				<div class="four wide column">
					<p>Feil på sidene? Kontakt <a href="mailto:Tarje.Lavik@uib.no">webredaktør</a>.</p>
				</div>
				<div class="nine wide column">
					<p><i class="creative commons icon"></i> Universitetsbiblioteket i Bergen deler Marcus datasettet fritt. Innholdet i Marcus er derimot ikke delt med CC0. Innhold vil bli merket med korrekt lisens.</p>
				</div>
			</div>
		</div>
	</div>
</footer>  
<script src="{{lodspk.home}}assets/semantic.min.js"></script>
<script src="{{lodspk.home}}js/marcus-ux.js"></script>