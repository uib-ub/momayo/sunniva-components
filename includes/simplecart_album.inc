<div class="simpleCart_shelfItem well" style="border-radius: 0; background-color: #fff;">
  <div class="ui grid">
    <div class="sixteen wide column">
      <h4>BESTILL FRA ALBUM {% for row in models.main %}
                  {% if row.title && row.title.value != "" %}
                    "{{row.title.value}}"
                  {% else %}
                    {% if row.label && row.label.value != "" %}
                      "{{row.label.value}}"
                    {% else %} 
                      {{row.identifier.value}}
                    {%endif%}
                  {%endif%}
                  {% if row.alternative %}
                    "{{row.alternative.value}}"
                  {% endif %}
              {% endfor %}</h4>
      
      <p>For å erverve publiseringsrettigheter eller kjøpe trykk av spesifikke sider eller bilder fra albumet <em><strong>
        {% for row in models.main %}
                  {% if row.title && row.title.value != "" %}
                    "{{row.title.value}}"
                  {% else %}
                    {% if row.label && row.label.value != "" %}
                      "{{row.label.value}}"
                    {% else %} 
                      {{row.identifier.value}}
                    {%endif%}
                  {%endif%}
                  {% if row.alternative %}
                    "{{row.alternative.value}}"
                  {% endif %}
              {% endfor %}
        </strong></em>, send e-post til <strong><a style="text-decoration: underline;" href="mailto:billed@ub.uib.no?subject=Bestilling: {{row.identifier.value}}">billed@ub.uib.no</a></strong>.
        <br /><br/>
        <span style="background-color: #e55343; color:#fff; padding: 20px;"><strong>OBS!</strong> Husk å oppgi tittel, signatur, og sidene det gjelder.</span>
      </p>
      
    </div>
  </div>
</div>