<div class="ui four doubling stackable cards">
  {% for row in models.events %}
  <div class="card">
    {% if row.logo %}
    <a class="ui small image" href="{{row.classHierarchyURI.value}}">
      <img src="{{row.logo.value}}"/>
    </a>
    {% else %}
    <a href="{{row.uri.value}}" class="ui small image">
      <img data-src="holder.js/100x100">
    </a>
    {% endif %}
    <div class="middle aligned content">
      <a class="header" href="{{row.classHierarchyURI.value}}">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a>
    </div>
  </div>
  {% endfor %}
</div>