{%if models.po|length != 0 %}
<div class="ui segment">
  <h2 class="ui center aligned header">
    Alle metadata
  </h2>
  <div class="ui basic center aligned segment">
    <div class="ui list">
      <div class="item">
        <div class="ui large green label">
          <i class="external square alternate icon"></i> 
          <a href="{{lodspk.publicEndpoint}}?query=DESCRIBE+%3C{{lodspk.this.value}}%3E" target=_blank>Gjør avanserte spørringer med Sparql (åpne data)</a> 
        </div>
      </div>

      {% if lodspk.isAdmin %}
      <div class="item">
        <div class="ui large red label">
          <i class="external icon"></i> <a href="{{lodspk.closedEndpoint}}?query=DESCRIBE+%3C{{lodspk.this.value}}%3E" target=_blank>Gjør avanserte spørringer med Sparql (lukkede data)</a> 
        </div>
      </div>
      {% endif %}
    </div>
  </div>

  <table style="border-top: solid" class="ui very compact very basic collapsing celled small definition table" about="{{uri}}">

    {% for row in models.po %}
    <tr>
      <td>
        {% ifchanged row.p.value %}
        <div style="margin-left: 0.5rem;">
          {% if row.classLabel %}<span>{{row.classLabel.curie}}</span>{% else %}<span>{{row.p.value}}{%endif%}
          <br>
          <span style="font-weight: normal;"><small><a href="{{row.p.value}}">({{row.p.value}})</a></small></span></span>
        </div>
        {% endifchanged%}
      
        {%if row.o.uri == 1 && row.label %}
        {% ifchanged row.o.value %}
        <td>
          <a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.label.value}}</a>
        </td>
        {% endifchanged%}
        {%endif%}

        {%if row.o.uri == 1 && !row.label %}
        <td>
          <a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.o.curie}}</a>
        </td>
        {%endif%}

        {%if row.o.uri == 0 && !row.label %}
        <td>
          <span property='{{row.p.value}}'>{{row.o.value}}</span>
        </td>
        {%endif%}
      </td>
    </tr>
    {% endfor %}
  </table>
</div>
{%endif%}
