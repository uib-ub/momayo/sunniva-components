  <button class="ui button" type="button" id="lastned">Last ned &nbsp; <i class="download icon"></i></button>
    <div class="ui modal lastned">
      <div class="lastned-content" style="padding: 20px; border-radius: 0; background-color: #fff;">
        <span style="font-weight: 600;">
          Velg størrelse:
          <br />
          {% if first.digitalresources.imgXS.value %}
            <div class="item"><a href="{{first.digitalresources.imgXS.value}}" download>JPG - veldig liten</a></div>
          {% endif %}
          <br />
          {% if first.digitalresources.imgSM.value %}
            <div class="item"><a href="{{first.digitalresources.imgSM.value}}" download>JPG - liten</a></div>
          {% endif %}
          <br />
          {% if first.digitalresources.imgMD.value %}
            <div class="item"><a itemprop="image" href="{{first.digitalresources.imgMD.value}}" download>JPG - medium</a></div>
          {% endif %}
          <br />
          {% if first.digitalresources.imgLG.value %}
            <div class="item"><a itemprop="image" href="{{first.digitalresources.imgLG.value}}" download>JPG - stor</a></div>
          {% endif %}
        </span>
        <div class="ui divider"></div>
        <strong>Bruksvilkår:</strong><br /><br />
        Alle bilder kan fritt lastes ned og brukes mot kreditering av fotograf og institusjon.
      </div>
    </div>

    <script>
      $("#lastned").click(function(){
        $(".lastned").modal('show');
      });
      $(".lastned").modal({
        closable: true
      });
    </script>