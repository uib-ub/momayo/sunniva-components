<?php
#$conf['endpoint']['local'] = 'http://127.0.0.1:3030/marcus-prod/query';
$conf['endpoint']['local'] = 'http://sparql.ub.uib.no/sparql/sparql';
$conf['endpoint']['bibsys'] = 'http://marcus.uib.no/sparql/bibsys-auth/query';
$conf['endpoint']['stat'] = 'http://marcus.uib.no/sparql/rdf-statistikk/sparql';
//$conf['endpoint']['dbpedia'] = 'http://dbpedia.org/sparql';
//$conf['endpoint']['dbpedia'] = 'http://live.dbpedia.org/sparql';
$conf['home'] = '/var/www/html/marcus/';

$conf['basedir'] = 'http://localhost/';
$conf['debug'] = true;
$conf['root'] = 'home';
//$conf['cache']['global'] = 1200;

$conf['ns']['base']   = 'http://data.ub.uib.no/';
$conf['ns']['local'] = 'http://data.ub.uib.no/';
$conf['ns']['scheme'] = 'http://marcus.uib.no/instance/charter/';
$conf['ns']['ubbinst'] ='http://data.ub.uib.no/instances/';
$conf['ns']['ubbont'] = 'http://data.ub.uib.no/ontology/';
$conf['ns']['dct'] = 'http://purl.org/dc/terms/';
$conf['ns']['rdf'] = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#';
$conf['ns']['rdfs'] = 'http://www.w3.org/2000/01/rdf-schema#';
$conf['ns']['text'] = 'http://jena.apache.org/text#';
$conf['ns']['skos'] = 'http://www.w3.org/2004/02/skos/core#';
$conf['ns']['foaf'] = 'http://xmlns.com/foaf/0.1/';
$conf['ns']['schema'] = 'http://schema.org/';
$conf['ns']['ubstat'] = 'http://data.ub.uib.no/ontology/ubstat#';
$conf['ns']['sdmx-attribute'] = 'http://purl.org/linked-data/sdmx/2009/attribute#';
$conf['ns']['sdmx-measure'] = 'http://purl.org/linked-data/sdmx/2009/measure#';
$conf['ns']['qb'] = 'http://data.ub.uib.no/ontology/ubstat#';

// $conf['mirror_external_uris'] = true;
$conf['mirror_external_uris'] = $conf['ns']['base'];

// Cherry-picked components (see https://github.com/alangrafu/lodspeakr/wiki/Reuse-cherry-picked-components-from-other-repositories)

// Variables in  can be used to store user info.
// For examples, 'title' will be used in the header.
// (You can forget about all conventions and use your own as well)
$lodspk['title'] = 'Marcus';
$lodspk['subtitle'] = 'Spesialsamlingen ved Universitetet i Bergen';
$lodspk['currentDate'] = date("D M d, Y");
$lodspk['yesterday'] = date("Y-m-d", time() - 60 * 60 * 24);

/* Oyvind spesialkrydder query post variabel som kan brukes over hele lodspkr */
/*$lodspk['query'] = $_POST['query'];
$lodspk['latest'] = $_POST['latest'];
*/

/*ATTENTION: By default this application is available to
 * be exported and copied (its configuration)
 * by others. If you do not want that,
 * turn the next option as false
 */
$conf['export'] = false;

$conf['static']['haanga'] = false;

$conf['modules']['available'] = array('static','service','uri','type','admin');
?>