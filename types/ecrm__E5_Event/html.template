<!DOCTYPE html>
<html ng-app="marcus" lang="no" prefix="og: http://ogp.me/ns#">
<head>
	{%include "../../includes/head_common.inc"%}
	{%include "../../includes/head_page.inc"%}
</head>
<body class="event" ng-controller="searchController">
	<button onclick="topFunction()" id="myBtn" title="Tilbake til toppen"><i class="big caret up icon"></i></button>
  	{%include "../../includes/sidebar.inc"%}
  	{% if lodspk.isAdmin %}    
    	{%include "../../includes/static_sidebar_left.inc"%}
  	{% endif %}
  
  <div class="pusher">
    {%include "../../includes/header.inc"%}
    {%include "../../includes/header_mobile.inc"%}

		<div class="main-content-wrapper">
			<div class="fullwidth">

				<div class="ui stackable grid">
					<div class="four wide column side">
						<div class="content-info" style="clear:both; display: block; min-height: 150px; margin-bottom: 20px;">
							<h2>{{ first.main.label.value }} </h2>
							<p>{{ first.description.descriptions.value }}</p>
							{% if first.main.logo != null %}
							<img class="ui image" src="{{first.main.logo.value}}">
							{% endif %}

							{% if first.main.begin.value != false || first.main.end.value != false %}
								{% if first.main.begin.value == first.main.end.value %}
									<p><strong>Dato</strong>: {{first.main.begin.value}}		
									{%else%}
									<strong>Start</strong>: {{first.main.begin.value}}<br>
									<strong>Slutt</strong>: {{first.main.end.value}}
									</p>
								{% endif %}
							{% endif %}

							{% if models.hierarchy|length != 0 %}
							<ul class="ui list">
								{% for row in models.hierarchy %}
								<li class="item">{% if forloop.first%}<i class="fa fa-archive"></i>{%else%}<i class="fa fa-chevron-up"></i>{% endif %} <a href="{{row.uri.value}}">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a></li>
								{% endfor %}
							</ul>
							{% endif %}		

							{% if models.webresources|length != 0  %}
							<p><strong>Nettsider</strong><br>
								{% for row in models.webresources %}
								<a target="_blank" href="{{row.uri.value}}">{{row.label.value}}</a> <i class="fa fa-external-link"></i><br>
								{% endfor %}
							</p>
							{% endif %}

							{% if models.ms|length != 0 %}
							<p><strong>{{ first.main.label.value }} er relatert til disse samlingene.</strong> Disse samlingene er ikke tilgjengelig i Marcus, men du kan finne mer informasjon i den gamle <a href="http://www.uib.no/filearchive/manuskriptkatalogen-ms-1-2097.pdf" target="_blank">Manuskriptkatalogen (pdf)</a>. 
							</p>
							<ul class="ui horizontal list">
								{% for row in models.ms %}
								<li class="item"><i class="fa fa-dot-circle-o"></i> {{row.label.value}}</li>
								{% endfor %}
							</ul>
							{% endif %}			
						</div>
						<!-- <div class="ui bottom attached tab white segment" data-tab="second" style="padding-left: 0px;">-->
							<div style="margin-top: 20px;">
								<search-instance-box></search-instance-box>
								<br>
								<div ng-cloak id="facet-sidebar" class="ng-cloak four wide column mobile or lower uihidden">
									<div class="">
										<facets></facets>
									</div>
								</div>
							</div>
						<!--</div>-->
					</div>

						<div class="twelve wide column">
							<div class="ui basic segment" style="padding: 0px;">
								{% if first.count.total.value > 100 %}
									<div class="ui borderless pagination menu">
										<script>
											if ("{{lodspk.args.arg1}}".length < 1)
												{page = 0;}
											else {page = "{{lodspk.args.arg1}}";}
											if ("{{lodspk.args.arg0}}".length > 0)
												{page=parseInt(page)}
											else {}
												var offset = 50;
											var count = {{first.count.total.value}};
											var totalPages = Math.floor(count / 50);
											var lastPage = '{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/'+ (count - ((count - (50*totalPages))));

											var nextPage = page+offset;
											var prevPage = page-offset;
											var next_uri = '{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/'+ nextPage;
											var prev_uri = '{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/'+ prevPage;
											if (prevPage === 0){
												document.write('<div class="item"><a href="{{lodspk.home}}instance/event/{{lodspk.args.arg0}}" id="prev"><i class="icon left arrow"></i> Forrige 50</a></div>'); 
											}   
											else if ("{{lodspk.args.arg1}}".length > 1){
												document.write('<div class="item"><a href="{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/">Første side</a></div><div class="item"><a href="" class="prev">Forrige 50</a></div>');
												$(".prev").attr("href",prev_uri);
											};

											if ({{first.count.total.value}} > nextPage){
												document.write('<div class="item"><a href="" class="next">Neste 50</a></div><div class="item"><a class="lastpage" href="">Siste side <i class="icon right arrow"></i></a></div>');
												$(".next").attr("href",next_uri);
												$(".lastpage").attr("href",lastPage);
											};
										</script> 
									</div>
								{% endif %}

								<script src="{{lodspk.home}}assets/search/vendor.min.js"></script>
	              <script src="{{lodspk.home}}assets/search/app.min.js"></script>
	              <br />
	              <instance-search-controller-wrapper></instance-search-controller-wrapper>

								<!-- problems minifying the following -->
								<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate-interpolation-messageformat/2.11.0/angular-translate-interpolation-messageformat.min.js" type="text/javascript"></script>
								<script src="http://monospaced.github.io/bower-qrcode-generator/js/qrcode.js"></script>

								<!--Constants for this particular page-->
								<script>
									angular.module("settings", [])
									.constant("pageSetting", {
										index: ["{{lodspk.SEARCH_INDEX}}"],
										appRoot: ["/assets/search/"],
										blackboxUrl: "{{lodspk.searchAPI}}",
										persistentFilterOptions: [
										{
											"urlParam": null,
											"filter": "producedIn.exact",
											"value": "{{first.main.label.value}}",
											"description": "Hendelse",
											"excludeFacets": [],
										}
										],
										facets: [
										{
											"field": "type.exact",
											"size": 30,
											"operator": "OR",
											"order": "count_desc",
											"min_doc_count": 0
										},
										{
											"field": "hasZoom",
											"size": 10,
											"operator": "AND",
											"order": "term_asc",
											"min_doc_count": 0
										},
										{
											"field": "isDigitized",
											"size": 10,
											"operator": "AND",
											"order": "count_desc",
											"min_doc_count": 0
										},
										{
											"field": "subject.exact",
											"size": 15,
											"operator": "AND",
											"min_doc_count": 1
										},
	                    /*{
	                      "field": "isPartOf.exact",
	                      "size": 15,
	                      "operator": "OR",
	                      "order": "count_desc",
	                      "min_doc_count": 1
	                  },*/
	                  {
	                  	"field": "producedIn.exact",
	                  	"size": 10,
	                  	"operator": "AND",
	                  	"order": "count_desc",
	                  	"min_doc_count": 1
	                  },
	                  {
	                  	"field": "maker.exact",
	                  	"size": 10,
	                  	"operator": "AND",
	                  	"order": "count_desc",
	                  	"min_doc_count": 1
	                  }
	                        //OBS: Supported intervals: weeks(w), days(d), hours(h), minutes(m), seconds(s)
	                        /*{
	                         "field": "created",
	                         "type" : "date_histogram",
	                         "format" : "yyyy",
	                         "interval" : "520w",
	                         "min_doc_count" : 1,
	                         "order" : "key_asc"
	                     }*/
	                     ],
	                     sortOptions: [
	                     { value : "", description : "filters.relevance" },
	                     { value : "identifier:asc", description : "filters.sortAsc" },
	                     { value : "identifier:desc", description : "filters.sortDesc" },
	                     { value : "available:asc", description : "filters.availableAsc" },
	                     { value : "available:desc", description : "filters.availableDesc" },
	                     { value : "dateSort:asc", description : "filters.dateAsc" },
	                     { value : "dateSort:desc", description : "filters.dateDesc" }
	                     ],
	                     tags: {
	                     	"maker" : { queryField: "maker.exact", responseField: "maker", icon: "user", css: "maker"},
	                     	"collection" : { queryField: "isPartOf.exact", responseField: "isPartOf", icon: "archive", css: "collection"},
	                     	"event" : { queryField: "producedIn.exact", responseField: "producedIn", icon: "archive", css: "collection"},
	                     	"spatial" : { queryField: "spatial.exact", responseField: "spatial", icon: "marker", css: "spatial"},
	                     	"subject" : { queryField: "subject.exact", responseField: "subject", icon: "tag", css: "subject"},
	                     	"type" : { queryField: "type.exact", responseField: "type", icon: "file outline", css: "type"},
	                     	"isDigitized" : { queryField: "isDigitized", responseField: "", icon: "", css: "status"},
	                     	"hasZoom" : { queryField: "hasZoom", responseField: "", icon: "", css: "status"},
	                     }
	                 });
	             	</script>
	             	{% if first.count.total.value > 100 %}
		             	<div class="ui borderless pagination menu">
			             	<script>
			             		if ("{{lodspk.args.arg1}}".length < 1)
			             			{page = 0;}
			             		else {page = "{{lodspk.args.arg1}}";}
			             		if ("{{lodspk.args.arg0}}".length > 0)
			             			{page=parseInt(page)}
			             		else {}
			             		var offset = 50;
			             		var count = {{first.count.total.value}};
			             		var totalPages = Math.floor(count / 50);
			             		var lastPage = '{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/'+ (count - ((count - (50*totalPages))));

			             		var nextPage = page+offset;
			             		var prevPage = page-offset;
			             		var next_uri = '{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/'+ nextPage;
			             		var prev_uri = '{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/'+ prevPage;
			             		if (prevPage === 0){
			             			document.write('<div class="item"><a href="{{lodspk.home}}instance/event/{{lodspk.args.arg0}}" id="prev"><i class="icon left arrow"></i> Forrige 50</a></div>'); 
			             		}   
			             		else if ("{{lodspk.args.arg1}}".length > 1){
			             			document.write('<div class="item"><a href="{{lodspk.home}}instance/event/{{lodspk.args.arg0}}/">Første side</a></div><div class="item"><a href="" class="prev">Forrige 50</a></div>');
			             			$(".prev").attr("href",prev_uri);
			             		};

			             		if ({{first.count.total.value}} > nextPage){
			             			document.write('<div class="item"><a href="" class="next">Neste 50</a></div><div class="item"><a class="lastpage" href="">Siste side <i class="icon right arrow"></i></a></div>');
			             			$(".next").attr("href",next_uri);
			             			$(".lastpage").attr("href",lastPage);
			             		};
		             		</script> 
		             	</div>
            		{% endif %}
         		</div>
     			</div>
 				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{lodspk.home}}js/backtotop/backtotop.js"></script>
  {%include "../../includes/footer.inc"%}
</body>
</html>
